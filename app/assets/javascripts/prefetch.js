(function() {
  var OldHttpRequest, preload,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    __hasProp = {}.hasOwnProperty;

  OldHttpRequest = Turbolinks.HttpRequest;

  Turbolinks.CachedHttpRequest = (function(_super) {
    __extends(CachedHttpRequest, _super);

    function CachedHttpRequest(_, location, referrer) {
      CachedHttpRequest.__super__.constructor.call(this, this, location, referrer);
    }

    CachedHttpRequest.prototype.requestCompletedWithResponse = function(response, redirectedToLocation) {
      this.response = response;
      return this.redirect = redirectedToLocation;
    };

    CachedHttpRequest.prototype.requestFailedWithStatusCode = function(code) {
      return this.failCode = code;
    };

    CachedHttpRequest.prototype.oldSend = function() {
      var _ref;
      if (this.xhr && !this.sent) {
        this.notifyApplicationBeforeRequestStart();
        this.setProgress(0);
        this.xhr.send();
        this.sent = true;
        return (_ref = this.delegate) != null ? typeof _ref.requestStarted === "function" ? _ref.requestStarted() : void 0 : void 0;
      }
    };

    CachedHttpRequest.prototype.send = function() {
      if (this.failCode) {
        return this.delegate.requestFailedWithStatusCode(this.failCode, this.failText);
      } else if (this.response) {
        return this.delegate.requestCompletedWithResponse(this.response, this.redirect);
      } else {
        return this.oldSend();
      }
    };

    return CachedHttpRequest;

  })(Turbolinks.HttpRequest);

  Turbolinks.HttpRequest = (function() {
    function HttpRequest(delegate, location, referrer) {
      var cache;
      cache = Turbolinks.controller.cache.get("prefetch" + location);
      if (cache) {
        Turbolinks.controller.cache["delete"]("prefetch" + location);
        console.log(JSON.stringify(Turbolinks.controller.cache.keys));
        cache.delegate = delegate;
        return cache;
      } else {
        return new OldHttpRequest(delegate, location, referrer);
      }
    }

    return HttpRequest;

  })();

  Turbolinks.SnapshotCache.prototype["delete"] = function(location) {
    var key;
    key = Turbolinks.Location.wrap(location).toCacheKey();
    return delete this.snapshots[key];
  };

  preload = function(event) {
    var cache, link, location, method, request;
    if (link = Turbolinks.controller.getVisitableLinkForNode(event.target)) {
      if (location = Turbolinks.controller.getVisitableLocationForLink(link)) {
        if (Turbolinks.controller.applicationAllowsFollowingLinkToLocation(link, location)) {
          if (((method = link.attributes["data-method"]) != null) && method.value !== 'get') {
            return;
          }
          if (location.anchor || location.absoluteURL.endsWith("#")) {
            return;
          }
          if (location.absoluteURL === window.location.href) {
            return;
          }
          cache = Turbolinks.controller.cache.get(location);
          if (!cache) {
            cache = Turbolinks.controller.cache.get("prefetch" + location);
          }
          if (!cache) {
            request = new Turbolinks.CachedHttpRequest(null, location, window.location);
            Turbolinks.controller.cache.put("prefetch" + location, request);
            return request.send();
          }
        }
      }
    }
  };

  document.addEventListener("touchstart", preload);

  document.addEventListener("mouseover", preload);

}).call(this);