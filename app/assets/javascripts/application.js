// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//


//= require classie
//= require jquery-min
//= require bootstrap
//= require jquery-ui
//= require jqueryForm
//= require jquery.validate
//= require jquery-ui-widget
//= require jquery-leanModal-min
//= require jquery-uls-data
//= require jquery-uls-data-utils
//= require jquery-uls-lcd
//= require jquery-uls-languagefilter
//= require jquery-uls-regionfilter
//= require jquery-uls-core
//= require jquery-flexisel
//= require jquery-flexslider
//= require responsiveslides-min
//= require move-top
//= require easing
//= require bootstrap-select
//= require easyResponsiveTabs
//= require sweetalert
//= require activestorage
//= require turbolinks
//= require jquery_ujs
//= require typeahead
//= require home
//= require prefetch
//= require timeago

$("#flexiselDemo3").flexisel({
    visibleItems: 1,
    animationSpeed: 1000,
    autoPlay: true,
    autoPlaySpeed: 5000,
    pauseOnHover: true,
    enableResponsiveBreakpoints: true,
    responsiveBreakpoints: {
        portrait: {
            changePoint: 480,
            visibleItems: 1
        },
        landscape: {
            changePoint: 640,
            visibleItems: 1
        },
        tablet: {
            changePoint: 768,
            visibleItems: 1
        }
    }
});


$(document).ready(function () {

    $("#show-area-select").click(function (e) {
        $("#dis-none").toggle();

    });

    $(".area_cont").click(function (e) {
        $(".dis-none").toggle();

    });

    $(".link_advance_search").click(function () {
        $("#short-search").hide()
        $("#advance-search").show()
        $(".link_advance_search").hide()
        $(".link_close_advance_search").show()
    });
    $(".link_close_advance_search").click(function () {
        $("#short-search").show()
        $("#advance-search").hide()
        $(".link_advance_search").show()
        $(".link_close_advance_search").hide()
    });


    // $(function () {
    //         $('select').selectpicker();
        // });

    // $(window).load(function () {

    $("#slider").responsiveSlides({
        auto: true,
        pager: false,
        nav: true,
        speed: 500,
        maxwidth: 800,
        namespace: "large-btns"
    });


    var mySelect = $('#first-disabled2');

    $('#special').on('click', function () {
        mySelect.find('option:selected').prop('disabled', true);
        mySelect.selectpicker('refresh');
    });

    $('#special2').on('click', function () {
        mySelect.find('option:disabled').prop('disabled', false);
        mySelect.selectpicker('refresh');
    });


    $('.uls-trigger').uls({
        onSelect: function (language) {
            var languageName = $.uls.data.getAutonym(language);
            $('.uls-trigger').text(languageName);
        },
        quickList: ['en', 'hi', 'he', 'ml', 'ta', 'fr'] //FIXME
    });


    // $(".scroll").click(function (event) {
    //     event.preventDefault();
    //     $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
    // });


////////  toggle menu if needed

    (function () {

        var bodyEl = document.body,
            content = document.querySelector('.content-wrap'),
            openbtn = document.getElementById('open-button'),
            closebtn = document.getElementById('close-button'),

            isOpen = false;

        function init() {
            initEvents();
        }

        function initEvents() {
            openbtn.addEventListener('click', toggleMenu);
            if (closebtn) {
                closebtn.addEventListener('click', toggleMenu);
            }

// close the menu element if the target it´s not the menu element or one of its descendants..
            /**content.addEventListener( 'click', function(ev) {
             var target = ev.target;
             if( isOpen && target !== openbtn ) {
             toggleMenu();
             }
             } ); */
        }

        function toggleMenu() {
            if (isOpen) {
                classie.remove(bodyEl, 'show-menu');
            }
            else {
                classie.add(bodyEl, 'show-menu');
            }
            isOpen = !isOpen;
        }

        init();

    })();


//    $('#e2').select2({
//        sortResults:function (results, container, query) {
//            if (query.term) {
//                use the built in javascript sort function
//                return results.sort(function (a, b) {
//                    if (a.text.length > b.text.length) {
//                        return 1;
//                    } else if (a.text.length < b.text.length) {
//                        return -1;
//                    } else {
//                        return 0;
//                    }
//                });
//            }
//            return results;
//        },
//
//        placeholder:"Select a Category",
//              minimumInputLength: 2,
//        allowClear:true

//    });


    $('#home-item').on('change', function () {

        var bla = $('#home-text').val();

        if (this.value == "" && !bla) {
            $("#home-btn").addClass('disabled');
        }
        else {

            $("#home-btn").removeClass('disabled');

        }

    });

    $("#home-text").keyup(function () {


        var bla = $('#home-item').val();

        if (this.value == "" && !bla) {
            $("#home-btn").addClass('disabled');
        }
        else {

            $("#home-btn").removeClass('disabled');

        }

    });


});


///     for turbolinks work properly


$(document).on("turbolinks:load", function () {

    // ##################################### home page js ################

    // $(function () {
    //         $('select').selectpicker();
        // });

    $("#slider").responsiveSlides({
        auto: true,
        pager: false,
        nav: true,
        speed: 500,
        maxwidth: 800,
        namespace: "large-btns"
    });

    $().UItoTop({easingType: 'easeOutQuart'});

    // $(".scroll").click(function (event) {
    //     event.preventDefault();
    //     $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
    // });

    $("#top-search > li > a").on("click", function (e) {

        if ($(this).parent().has("ul")) {
            e.preventDefault();
        }
        if (!$(this).hasClass("open")) {
            // hide any open menus and remove all other classes
            $("#top-search li ul").slideUp(350);
            $("#top-search li a").removeClass("open");

            // open our new menu and add the open class
            $(this).next("ul").slideDown(350);
            $(this).addClass("open");
        }

        else if ($(this).hasClass("open")) {
            $(this).removeClass("open");
            $(this).next("ul").slideUp(350);
        }
    });


    $(".dropdown-toggle1").click(function () {
        $('.side-bar').slideToggle();
    });

    $(".for-show").click(function () {
        $(".region_main").slideToggle();
    });


    $('.tst').on('click', function () {
        $('#ali').text($(this).text());
        $('#sub_category_id').val($(this).attr('id'));
        $('.tst').removeClass('subCat')
        $(this).addClass('subCat')
        // $('.side-bar').slideToggle();
        // $(".region_main").slideToggle();
    });


    var movies = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/home/autocomplete_json?query=Q',
            wildcard: 'Q'
        }
    });

    $('#prefetch .typeahead').typeahead(null, {
        name: 'query',
        limit: 10,
        minLength: 2,
        source: movies
    });

    $('#prefetch .typeahead').on(
        {
            'typeahead:selected': function (e, datum) {
                $('#outer_city').val($('#query').val());
                $(".region_main").slideToggle();
            },
            'typeahead:autocompleted': function (e, datum) {
                console.log(datum);
                console.log('tabbed');
            }
        });


    var movies1 = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/home/autocomplete_json1?query1=Q1',
            wildcard: 'Q1'
        }
    });


    $('#mainfetch .typeahead').typeahead(null, {
        name: 'query1',
        limit: 10,
        minLength: 2,
        source: movies1
    });



    var movies3 = new Bloodhound({

           datumTokenizer: Bloodhound.tokenizers.whitespace,
           queryTokenizer: Bloodhound.tokenizers.whitespace,
           remote: {
               url: '/property/autocomplete_society?query3=Q3',
               wildcard: 'Q3',
               replace: function(url, uriEncodedQuery) {
                             val = $('#zameen-main').find(":selected").val();
                             if (!val) return url.replace("Q3",uriEncodedQuery);
                             return url.replace("Q3",uriEncodedQuery) + '&slug=' + encodeURIComponent(val)
                           }
           }
       });


       $('#zameen-society .typeahead').typeahead(null, {
           name: 'query3',
           limit: 10,
           minLength: 2,
           source: movies3
       });


    var movies33 = new Bloodhound({

           datumTokenizer: Bloodhound.tokenizers.whitespace,
           queryTokenizer: Bloodhound.tokenizers.whitespace,
           remote: {
               url: '/property/autocomplete_society?query3=Q3',
               wildcard: 'Q3',
               replace: function(url, uriEncodedQuery) {
                             val = $('#zameen-main-rent').find(":selected").val();
                             if (!val) return url.replace("Q3",uriEncodedQuery);
                             return url.replace("Q3",uriEncodedQuery) + '&slug=' + encodeURIComponent(val)
                           }
           }
       });


       $('#zameen-society-rent .typeahead').typeahead(null, {
           name: 'query3',
           limit: 10,
           minLength: 2,
           source: movies33
       });

    var movies4 = new Bloodhound({

               datumTokenizer: Bloodhound.tokenizers.whitespace,
               queryTokenizer: Bloodhound.tokenizers.whitespace,
               remote: {
                   url: '/property/autocomplete_society?query4=Q4',
                   wildcard: 'Q4',
                   replace: function(url, uriEncodedQuery) {
                                 val1 = $('#zameen-main-detail').find(":selected").val();
                                 if (!val1) return url.replace("Q4",uriEncodedQuery);
                                 return url.replace("Q4",uriEncodedQuery) + '&slug=' + encodeURIComponent(val1)
                               }
               }
           });


           $('#zameen-society-detail .typeahead').typeahead(null, {
               name: 'query4',
               limit: 10,
               minLength: 2,
               source: movies4
           });

    var movies44 = new Bloodhound({

                   datumTokenizer: Bloodhound.tokenizers.whitespace,
                   queryTokenizer: Bloodhound.tokenizers.whitespace,
                   remote: {
                       url: '/property/autocomplete_society?query4=Q4',
                       wildcard: 'Q4',
                       replace: function(url, uriEncodedQuery) {
                                     val1 = $('#zameen-main-detail-rent').find(":selected").val();
                                     if (!val1) return url.replace("Q4",uriEncodedQuery);
                                     return url.replace("Q4",uriEncodedQuery) + '&slug=' + encodeURIComponent(val1)
                                   }
                   }
               });


               $('#zameen-society-detail-rent .typeahead').typeahead(null, {
                   name: 'query4',
                   limit: 10,
                   minLength: 2,
                   source: movies44
               });


    $('.city-home').on('click', function () {
        $('#outer_city').val($(this).text());
        $('#city_id').val($(this).attr('id'));
        $(".region_main").slideToggle();
    });

    $('#inner-add').on('click', function () {
        $('#outer_city').val($('#query').val());
        $(".region_main").slideToggle();
    });


    $('.tst').on('click', function () {
        $('#ali').text($(this).text());
        $('#sub_category_id').val($(this).attr('id'));
        $('.side-bar').slideToggle();
        // $(".region_main").slideToggle();
    });


// ############################################# register  and login


    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
    }, "Only alphabetical characters");

    jQuery.validator.addMethod("phoneno", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.match(/^\+92(?:[0-9] ?){9,13}[0-9]$/);
    }, "Phone number should start with +92 and must be like +923237979346. Also space should not in start or in any place");


    $("#sign-up-in").validate({

        rules: {

            "phone": {
                required: true,
                phoneno: true,
                minlength: 13,
                maxlength: 13
            },
//            "term": {
//                required: true
//            },
            "name": {
                required: true,
                lettersonly: true,
                minlength: 3
            },

            "description": {
                required: true,
                minlength: 10,
                maxlength: 3500
            },

            "user[email]": {
                required: true,
                email: true
            },
            "user[password]": {
                required: true,
                minlength: 6
            },
            "user[password_confirmation]": {
                required: true,
                minlength: 6
            },
            "term": {
                required: true
            }
        },
        messages: {

            "name": {
                required: "Please enter a name",
                lettersonly: "Name can be only alphbateic letter",
            },
            "phone": {
                required: "Please enter mobile phone"
                // number: "it should be in number like +923237979346"
            },
            "term": {required: "Please accept our policy"}
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");

            // Add `has-feedback` class to the parent div.form-group
            // in order to add icons to inputs
            element.parents(".col-sm-12").addClass("has-feedback");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }

            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if (!element.next("span")[0]) {
                $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
            }
        },
        success: function (label, element) {
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if (!$(element).next("span")[0]) {
                $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-12").addClass("has-error").removeClass("has-success");
            $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-12").addClass("has-success").removeClass("has-error");
            $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
        }
    });


// ################################################### Item show ####################

    $('.timeago').timeago();

    $('.flexslider').flexslider({
        animation: "slide",
        controlNav: "thumbnails"
    });


// ################################################ Item new ########################

    $('#state').on('change', function () {
        $('#spin-pro').css("display", "block");
        var container11 = $("#cities");
        if (this.value == "") {
            $('#cities').css("display", "none");
            $('#societies').css("display", "none");
            $('#phases').css("display", "none");
            $('#blocks').css("display", "none");
            $('#spin-pro').css("display", "none");
        }
        else {

            $('#societies').css("display", "none");
            $('#phases').css("display", "none");
            $('#blocks').css("display", "none");
            $('#spin-pro').css("display", "none");

            $.ajax({
                url: '/items/show_cities?state_id=' + this.value,
                type: 'get',
                dataType: 'html',
                processData: false,
                success: function (data) {
                    $('#spin-pro').css("display", "none");
                    document.getElementById('cities').style.display = "block";
                    container11.html(data);
                }
            });
        }

    });

    $('#category').on('change', function () {

        var container = $("#sub_cat");
        $('#spin-cat').css("display", "block");

        if (this.value == "") {
            $('#spin-cat').css("display", "none");
            // $(this).next("span").remove();
            // $(this).next("span").remove();

            document.getElementById('sub_cat').style.display = "none";
            document.getElementById('sub_cat_prod').style.display = "none";
            document.getElementById('price-condition').style.display = "none";
            document.getElementById('animals').style.display = "none";
            document.getElementById('jobs').style.display = "none";
            document.getElementById('bike-car-sub-cat').style.display = "none";
            document.getElementById('property-sub-cat').style.display = "none";
            document.getElementById('bike-car').style.display = "none";
            document.getElementById('marriage').style.display = "none";
            $('#societies').css("display", "none");
            // document.getElementById('item-photos').style.display = "block";
        }
        else {

            if ($(this).parents(".col-sm-12").hasClass("has-error")) {
                // $(this).parents(".col-sm-12").addClass("has-success").removeClass("has-error");
                // $(this).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
            }
            else {
                // $(this).parents(".col-sm-12").addClass("has-feedback has-success");
                // $(this).parents(".col-sm-12").append("<span class='glyphicon form-control-feedback glyphicon-ok'></span>");
            }


            var a = document.getElementById('category-error');
            if (a !== null) {
                // document.getElementById('category-error').style.display = "none";
            }

            if (this.value == "Mobiles" || this.value == "Electronics & Home Appliances" ||
                this.value == "Furniture & Home Decor" || this.value == "Books, Sports & Hobbies" ||
                this.value == "Kids" || this.value == "Fashion & Beauty") {
                document.getElementById('price-condition').style.display = "block";
                document.getElementById('animals').style.display = "none";
                document.getElementById('jobs').style.display = "none";
                document.getElementById('bike-car').style.display = "none";
                document.getElementById('property').style.display = "none";
                document.getElementById('bike-car-sub-cat').style.display = "none";
                document.getElementById('property-sub-cat').style.display = "none";
                document.getElementById('marriage').style.display = "none";
                $('#societies').css("display", "none");
                // document.getElementById('item-photos').style.display = "block";
            }
            else if (this.value == "Animals" || this.value == "Business, Industrial & Agriculture") {
                document.getElementById('animals').style.display = "block";
                document.getElementById('price-condition').style.display = "none";
                document.getElementById('jobs').style.display = "none";
                document.getElementById('bike-car').style.display = "none";
                document.getElementById('property').style.display = "none";
                document.getElementById('bike-car-sub-cat').style.display = "none";
                document.getElementById('property-sub-cat').style.display = "none";
                document.getElementById('marriage').style.display = "none";
                $('#societies').css("display", "none");
                // document.getElementById('item-photos').style.display = "block";
            }
            else if (this.value == "Jobs") {
                document.getElementById('jobs').style.display = "block";
                document.getElementById('animals').style.display = "none";
                document.getElementById('price-condition').style.display = "none";
                document.getElementById('bike-car').style.display = "none";
                document.getElementById('property').style.display = "none";
                document.getElementById('bike-car-sub-cat').style.display = "none";
                document.getElementById('property-sub-cat').style.display = "none";
                document.getElementById('marriage').style.display = "none";
                $('#societies').css("display", "none");
                // document.getElementById('item-photos').style.display = "none";

            }
            else if (this.value == "Services") {
                // do nothing currently. will done in future  Insha ALLAH

                document.getElementById('jobs').style.display = "none";
                document.getElementById('animals').style.display = "none";
                document.getElementById('price-condition').style.display = "none";
                document.getElementById('bike-car').style.display = "none";
                document.getElementById('property').style.display = "none";
                document.getElementById('bike-car-sub-cat').style.display = "none";
                document.getElementById('property-sub-cat').style.display = "none";
                document.getElementById('marriage').style.display = "none";
                $('#societies').css("display", "none");
                // document.getElementById('item-photos').style.display = "none";
            }

            else if (this.value == "Property for Sale" || this.value == "Property for Rent") {

                document.getElementById('jobs').style.display = "none";
                document.getElementById('animals').style.display = "none";
                document.getElementById('price-condition').style.display = "none";
                document.getElementById('bike-car').style.display = "none";
                document.getElementById('property').style.display = "block";
                document.getElementById('bike-car-sub-cat').style.display = "none";
                document.getElementById('property-sub-cat').style.display = "none";
                document.getElementById('marriage').style.display = "none";
                // document.getElementById('item-photos').style.display = "block";

            }

            else if (this.value == "Vehicles" || this.value == "Bikes") {

                document.getElementById('price-condition').style.display = "none";
                document.getElementById('animals').style.display = "none";
                document.getElementById('jobs').style.display = "none";
                document.getElementById('property').style.display = "none";
                document.getElementById('bike-car').style.display = "block";
                document.getElementById('bike-car-sub-cat').style.display = "none";
                document.getElementById('property-sub-cat').style.display = "none";
                document.getElementById('marriage').style.display = "none";
                $('#societies').css("display", "none");
                // document.getElementById('item-photos').style.display = "block";
            }

            else if (this.value == "Marriages") {

                document.getElementById('price-condition').style.display = "none";
                document.getElementById('animals').style.display = "none";
                document.getElementById('jobs').style.display = "none";
                document.getElementById('property').style.display = "none";
                document.getElementById('bike-car').style.display = "none";
                document.getElementById('bike-car-sub-cat').style.display = "none";
                document.getElementById('property-sub-cat').style.display = "none";
                document.getElementById('marriage').style.display = "block";
                $('#societies').css("display", "none");
                // document.getElementById('item-photos').style.display = "block";
            }

            //            else{
            //                document.getElementById('price-condition').style.display = "none";
            //            }

            document.getElementById('sub_cat').style.display = "none";
            document.getElementById('sub_cat_prod').style.display = "none";
            $.ajax({
                url: '/items/show_sub_category?category_name=' + this.value,
                type: 'get',
                dataType: 'html',
                processData: false,
                success: function (data) {
                    $('#spin-cat').css("display", "none");
                    document.getElementById('sub_cat').style.display = "block";
                    container.html(data);
                }
            });

        }
    });


    // $("#category").select2({
    //     placeholder: "Choose a major Category",
    //     allowClear: true
    // });

    // $('#category').css({"display": "block", 'position': 'absolute', 'top': '3px', 'z-index': '-1'});

    // $("#city").select2({
    //     placeholder: "Choose a City",
    //     allowClear: true
    // });


    // $("#sub_category_id").select2({
    //     placeholder: "Choose a category",
    //     allowClear: true
    // });

    // $("#slider").responsiveSlides({
    //     auto: true,
    //     pager: false,
    //     nav: true,
    //     speed: 500,
    //     maxwidth: 800,
    //     namespace: "large-btns"
    // });


    $("#flexiselDemo3").flexisel({
        visibleItems: 1,
        animationSpeed: 1000,
        autoPlay: true,
        autoPlaySpeed: 5000,
        pauseOnHover: true,
        enableResponsiveBreakpoints: true,
        responsiveBreakpoints: {
            portrait: {
                changePoint: 480,
                visibleItems: 1
            },
            landscape: {
                changePoint: 640,
                visibleItems: 1
            },
            tablet: {
                changePoint: 768,
                visibleItems: 1
            }
        }
    });


});





