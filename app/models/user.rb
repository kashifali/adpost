class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         # :recoverable, :rememberable, :validatable, :confirmable, :omniauthable , omniauth_providers: %i[facebook]
         :recoverable, :rememberable, :validatable, :confirmable, :omniauthable , omniauth_providers: [:google_oauth2]

  belongs_to :role

  has_one :profile
  # accepts_nested_attributes_for :profile
  has_many :favourite_items


  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.role = Role.find_by_name("user")
      # user.name = auth.info.name   # assuming the user model has a name

      # user.image = auth.info.image # assuming the user model has an image
      # If you are using confirmable and the provider(s) you use validate emails,
      # uncomment the line below to skip the confirmation emails.
      user.skip_confirmation!
    end
  end




end
