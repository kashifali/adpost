class PhaseSector < ApplicationRecord
  extend FriendlyId

  friendly_id :name, use: :slugged, slug_column: :phase_slug
  belongs_to :society
  belongs_to :city
  has_many :blocks
  validates_uniqueness_of :name



end
