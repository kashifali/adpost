class Category < ApplicationRecord

  extend FriendlyId
  friendly_id :name, use: :slugged
  has_many :sub_categories
  has_many :items
  validates_uniqueness_of :name

  default_scope {order("name DESC")}

end
