class SubCategory < ApplicationRecord

  extend FriendlyId
  friendly_id :name, use: :slugged

  default_scope {order("name ASC")}
  belongs_to :category
  has_many :products
  has_many :items
  validates_uniqueness_of :name


  def category_and_sub_category_name
    "#{self.category.name}  ----   #{name}"
  end



end
