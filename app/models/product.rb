class Product < ApplicationRecord
  belongs_to :sub_category
  validates_uniqueness_of :name
  has_many :items
end
