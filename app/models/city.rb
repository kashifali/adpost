class City < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  belongs_to :province
  has_many :items
  has_many :phase_sectors
  has_many :societies
  has_many :blocks
  default_scope {order("name ASC")}





    # geocoded_by :name # address is an attribute of MyModel

    # or with a method
    # geocoded_by :full_address # full_address is a method which take some model's attributes to get a formatted address for example
  
    # the callback to set longitude and latitude
    # after_validation :geocode

    # the full_address method
    # def full_address
    #   "#{address}, #{zipcode}, #{city}, #{country}"
    # end



end

#  belongs_to :product, optional: true
