class Item < ApplicationRecord
  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  #TODO need to implement filters for marriages in future INSHALLAH

  #searchkick

  scope :sub_category_id, -> sub_category_id { where(sub_category_id: sub_category_id) }
  scope :property_sale, -> property_sale { where(sub_category_id: property_sale) }
  scope :property, -> property { where(category_id: property) }
  scope :property_rent, -> property_rent { where(sub_category_id: property_rent) }
  scope :city_id, -> city_id { where(city_id: city_id) }
  scope :beds, -> beds { where(bed_rooms: beds) }
  scope :baths, -> baths { where(bath_rooms: baths) }
  scope :outer_city, -> outer_city { where("city_id = ? and category_id != ? and category_id != ?", City.find_by_name(outer_city.strip).id,13,14) } # 13,14 ids is of propertis. will include only in property portal
  scope :society, -> society { where("society_id = ?", Society.find_by_name(society.strip).id) }
  scope :q, -> q { where("sub_category_id = ?", SubCategory.find_by_id(q.strip).id) }
  scope :min_price, -> min_price { where("price  > ?", min_price.to_i) }
  scope :max_price, -> max_price { where("price  < ?", max_price.to_i) }
  scope :area_unit, -> area_unit { where("area_unit  = ?", area_unit.strip) }
  scope :min_area, -> min_area { where("area  > ?", min_area.to_i) }
  scope :max_area, -> max_area { where("area  < ?", max_area.to_i) }
  scope :min_drive, -> min_drive { where("vehicle_drive_in_km  > ?", min_drive.to_i) }
  scope :max_drive, -> max_drive { where("vehicle_drive_in_km < ?", max_drive.to_i) }
  scope :home_word, -> home_word { where("lower(name) LIKE ? and category_id != ? and category_id != ?", "%#{home_word.split("in categories").first.strip.downcase}%",13,14) }
  scope :fuel_type, -> fuel_type { where("vehicle_fuel_type = ?",fuel_type) }
  scope :condition, -> condition { where("vehicle_condition = ?",condition) }
  scope :salary_period, -> salary_period { where("job_salary_period = ?",salary_period) }
  scope :job_position, -> job_position { where("job_position_type = ?",job_position) }
  scope :min_from, -> min_from { where("job_salary_from  > ?", min_from.to_i) }
  scope :max_from, -> max_from { where("job_salary_from < ?", max_from.to_i) }
  scope :min_to, -> min_to { where("job_salary_to > ?", min_to.to_i) }
  scope :max_to, -> max_to { where("job_salary_to < ?", max_to.to_i) }






  serialize :facilities

  enum status: [:inactive,:active,:pending]

  belongs_to :user
  belongs_to :city
  belongs_to :society, optional: true
  belongs_to :sub_category
  belongs_to :category
  belongs_to :product, optional: true

  has_many :favourite_items

  has_many :item_attachments
  accepts_nested_attributes_for :item_attachments


  def get_values(rec_hash={})
    item_val = rec_hash.each {|key, value| rec_hash.delete(key) if value.blank? || value == "default"}
    item_val.values.first
  end

  def slug_candidates
    [
        :name,
        [:name, :price],
        [:name, :price, Time.now.strftime("%Y-%d-%m:%S")]
    ]
  end



end
