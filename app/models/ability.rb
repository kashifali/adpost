class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
      user ||= User.new # guest user (not logged in)
      if user.role.present? and user.role.name == "Admin"
        can :manage, :all
      elsif user.role.present? and user.role.name == "sub_admin"
        can [:create,:show_sub_category,:show_cities,:show_sub_category_product, :read,:like_dislike,:like_dislike_index,:account_setting,:update_profile,:change_password,:update_password,:show_societies], [Item]
        can [:show_content,:all_categories,:index,:about,:privacy,:terms,:contact,:autocomplete_json,:autocomplete_json1,:contact_us,:user_items,:faq,:sitemap],:home
        can [:property_for_sale_or_rent,:list_city_property,:autocomplete_society,:list_city_society_property],:property
        can [:read,:show_phases,:show_blocks],[Block]
      else
        # can :read, Item
        can [:create,:show_sub_category,:show_cities,:show_sub_category_product, :read,:like_dislike,:like_dislike_index,:account_setting,:update_profile,:change_password,:update_password,:show_societies], [Item]
        can [:show_content,:all_categories,:index,:about,:privacy,:terms,:contact,:autocomplete_json,:autocomplete_json1,:contact_us,:user_items,:faq,:sitemap],:home
        can [:property_for_sale_or_rent,:list_city_property,:autocomplete_society,:list_city_society_property],:property
        can [:read,:show_phases,:show_blocks],[Block]
      end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
