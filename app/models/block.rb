class Block < ApplicationRecord
  extend FriendlyId

  friendly_id :name, use: :slugged, slug_column: :block_slug
  belongs_to :phase_sector
  belongs_to :city
  belongs_to :society

  validates_uniqueness_of :name

  default_scope {order("name ASC")}

end
