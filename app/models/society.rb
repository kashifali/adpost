class Society < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  validates_uniqueness_of :name
  belongs_to :city
  has_many :items
  has_many :phase_sectors
  has_many :blocks
  default_scope {order("name ASC")}
end
