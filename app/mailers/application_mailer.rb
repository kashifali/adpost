class ApplicationMailer < ActionMailer::Base
  # default from: 'from@example.com'
  layout 'mailer'

  def contact_us(mail,name,detail)
    @detail = detail
    @name = name
    @email = mail
    mail(to: "kashif.tufail17@gmail.com",from: mail, subject: 'Contact Email Adstous')
  end

end
