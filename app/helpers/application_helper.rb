module ApplicationHelper

  def find_favourite_by_user(item)
    @favourite = FavouriteItem.where("item_id = ? and user_id = ? ", item.id , current_user.id).last if current_user.present?
  end

end
