class SearchController < ApplicationController

  has_scope :sub_category_id
  has_scope :city_id
  has_scope :outer_city
  has_scope :home_word
  has_scope :q
  has_scope :min_price
  has_scope :min_drive
  has_scope :max_price
  has_scope :max_drive
  has_scope :condition
  has_scope :fuel_type
  has_scope :salary_period
  has_scope :job_position
  has_scope :min_from
  has_scope :max_from
  has_scope :min_to
  has_scope :max_to

  def index
    sub_category = SubCategory.find_by_id(params[:q]) ||
            SubCategory.find_by_slug(params["item"]) ||
            SubCategory.find_by_id(params[:sub_category_id].to_i)
        @sub_category = sub_category
        @category = sub_category.try(:category)
        @sub_categories = SubCategory.all
        @popular_city = City.all
        @bred_crumbs = sub_category
        @id = sub_category.try(:id) || params[:sub_category_id]
        params[:outer_city] = '' if params[:outer_city] == "All Pakistan" #for blank outer city searching like default
        @city_id = City.find_by_name(params[:outer_city]).try(:id) || params[:city_id]
        # binding.pry
        word = params[:home_word].split("in categories").first.strip.downcase if params[:home_word].present?
        @word = word

     @items =  apply_scopes(Item).all.order('created_at DESC').page(params[:page]).per(15)



    if sub_category.present?
      @page_title = "Free classifieds ads for #{sub_category.try(:name)} for sale in Pakistan - Adstous"
    else
      @page_title = "Free classifieds ads for #{params[:outer_city]} in Pakistan - Adstous"
    end
    @page_description = "Using Adstous Pakistan, you can post classified ads in #{sub_category.try(:name)}, Post unlimited classified ads in different categories like mobile phones, tablets,cars, electronics goods, bikes, houses, lands, plots, plot files,
                animals, property, business,services, furniture,birds, jobs, pets"

    respond_to do |format|
          format.html
          format.js
        end

  end



end


# 14134857328