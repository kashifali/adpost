class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def facebook
    auth = request.env["omniauth.auth"]

    u = User.find_by_uid(auth.uid)


    if u.present?
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Facebook"
      #sign_in_and_redirect '/', :event => :authentication
      sign_in_and_redirect u, event: :authentication #this will throw if @user is not activated

    else
      @user = find_for_facebook_oauth(auth, current_user)
      #session[:image] = auth.info.image
      #@social_data = SocialData.create(:uid => auth.uid, :provider => auth.provider, :token => auth.credentials.token, :image => auth.info.image, :url => auth.extra.raw_info.link)
      #session[:social_ids] << @social_data.id
      #session[:facebook] = "facebook"
      #flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Facebook"
      #session[:email] = @user.email
      session[:user] = @user
      redirect_to new_user_registration_url
    end


    #@user.password = Devise.friendly_token[0,20]

    #@user.save


  end

  ####################### with social login just login in to user without giving passwords

  def google_oauth2

    auth = request.env["omniauth.auth"]

    @user = User.from_omniauth(auth)
    if @user.persisted?
      unless @user.profile.present?
        Profile.create(:user_id => @user.id ,:name => auth.info.name  )
      end
      sign_in_and_redirect @user, event: :authentication #this will throw if @user is not activated
      set_flash_message(:notice, :success, kind: "Google") if is_navigational_format?
    else
      flash[:error] = []
      @user.errors.full_messages.each { |error| flash[:error] << error }
      # session["devise.google_data"] = request.env["omniauth.auth"]
      redirect_to register_url
    end

    
  end

  def failure
    redirect_to root_path
  end



  private

  def find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    unless user
      user = User.new(
          #:url_facebook => auth.extra.raw_info.link,
          :provider => auth.provider,
          :uid => auth.uid,
          :email => auth.info.email
      )
    end
    session[:name] = auth.extra.raw_info.name
    user
  end


end


