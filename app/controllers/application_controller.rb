class ApplicationController < ActionController::Base
  #binding.pry
  before_action :load_city_and_sub_categories
  before_action :get_domain
  # check_authorization

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  def load_city_and_sub_categories
    # session['society'] = ''
    @categories = Category.all
    @provinces = Province.all
    @url = request.url
    connection = ActiveRecord::Base.connection

    houses_for_sale_all_cities_with_count = "select ci.name,ci.slug,sc.id,count(*) as count from items as I

                                                        Inner Join cities as ci on ci.id = I.city_id

                                                        Inner Join sub_categories as sc on sc.id = I.sub_category_id

                                                        Inner Join societies as so on so.id = I.society_id

                 Where I.society_id IS NOT NULL and sc.category_id = 13 and sc.id = 95

                                                        group by ci.name,ci.slug,sc.id
    order by count DESC"

    houses_count_according_socities_in_lhr = "select so.name,sc.id,ci.slug as city_slug,so.slug as society_slug,count(*) as count from items as I

        Inner Join cities as ci on ci.id = I.city_id
                                                        Inner Join sub_categories as sc on sc.id = I.sub_category_id

                                                        Inner Join societies as so on so.id = I.society_id

                 Where I.society_id IS NOT NULL and sc.category_id = 13 and sc.id = 95 and I.city_id = 171

                                                        group by so.name,ci.slug,so.slug,sc.id

                                                        order by count DESC"

    houses_count_according_socities_in_kar = "select so.name,sc.id,ci.slug as city_slug,so.slug as society_slug,count(*) as count from items as I

            Inner Join cities as ci on ci.id = I.city_id
                                                            Inner Join sub_categories as sc on sc.id = I.sub_category_id

                                                            Inner Join societies as so on so.id = I.society_id

                     Where I.society_id IS NOT NULL and sc.category_id = 13 and sc.id = 95 and I.city_id = 227

                                                            group by so.name,ci.slug,so.slug,sc.id

                                                            order by count DESC"

    houses_count_according_socities_in_pindi = "select so.name,sc.id,ci.slug as city_slug,so.slug as society_slug,count(*) as count from items as I

            Inner Join cities as ci on ci.id = I.city_id
                                                            Inner Join sub_categories as sc on sc.id = I.sub_category_id

                                                            Inner Join societies as so on so.id = I.society_id

                     Where I.society_id IS NOT NULL and sc.category_id = 13 and sc.id = 95 and I.city_id = 199

                                                            group by so.name,ci.slug,so.slug,sc.id

                                                            order by count DESC"



    res_plots_count_according_socities_in_lhr = "select so.name,sc.id,ci.slug as city_slug,so.slug as society_slug,count(*) as count from items as I

            Inner Join cities as ci on ci.id = I.city_id
                                                            Inner Join sub_categories as sc on sc.id = I.sub_category_id

                                                            Inner Join societies as so on so.id = I.society_id

                     Where I.society_id IS NOT NULL and sc.category_id = 13 and sc.id = 130 and I.city_id = 171

                                                            group by so.name,ci.slug,so.slug,sc.id

                                                            order by count DESC"

    res_plots_count_according_socities_in_kar = "select so.name,sc.id,ci.slug as city_slug,so.slug as society_slug,count(*) as count from items as I

                Inner Join cities as ci on ci.id = I.city_id
                                                                Inner Join sub_categories as sc on sc.id = I.sub_category_id

                                                                Inner Join societies as so on so.id = I.society_id

                         Where I.society_id IS NOT NULL and sc.category_id = 13 and sc.id = 130 and I.city_id = 227

                                                                group by so.name,ci.slug,so.slug,sc.id

                                                                order by count DESC"

    res_plots_count_according_socities_in_pindi = "select so.name,sc.id,ci.slug as city_slug,so.slug as society_slug,count(*) as count from items as I

                Inner Join cities as ci on ci.id = I.city_id
                                                                Inner Join sub_categories as sc on sc.id = I.sub_category_id

                                                                Inner Join societies as so on so.id = I.society_id

                         Where I.society_id IS NOT NULL and sc.category_id = 13 and sc.id = 130 and I.city_id = 199

                                                                group by so.name,ci.slug,so.slug,sc.id

                                                                order by count DESC"


    @houses_sale_cities_with_count = connection.exec_query(houses_for_sale_all_cities_with_count);
    @houses_with_count_lhr = connection.exec_query(houses_count_according_socities_in_lhr);
    @houses_with_count_kar = connection.exec_query(houses_count_according_socities_in_kar);
    @houses_with_count_pindi = connection.exec_query(houses_count_according_socities_in_pindi);
    @res_plots_with_count_lhr = connection.exec_query(res_plots_count_according_socities_in_lhr);
    @res_plots_with_count_kar = connection.exec_query(res_plots_count_according_socities_in_kar);
    @res_plots_with_count_pindi = connection.exec_query(res_plots_count_according_socities_in_pindi);

    # binding.pry
  end

  def get_category
    if request.url.include? "property-sale"
      @str = "property-sale"
      cat_id = Category.find_by_slug("property-for-sale").try(:id)
      sub_cat_id = SubCategory.find_by_slug("houses").try(:id)
    else
      @str = "property-rent"
      cat_id = Category.find_by_slug("property-for-rent").try(:id)
      sub_cat_id = SubCategory.find_by_slug("houses-rent").try(:id)
    end
    return cat_id, sub_cat_id
  end

  private
  def get_domain

    # binding.pry
    # blogs = Blog.where(subdomain: request.subdomain)

    # if blogs.count > 0
    # nnnnnnnnn
    # @blog = blogs.first
    # if request.url.include? 'property' and request.subdomain == 'www' and request.subdomain != 'zameen' #for heroku
    if request.url.include? 'property' and request.subdomain != 'zameen' #for local
      # ppppppp
      # redirect_to root_url(subdomain: 'zameen')
    else
      # redirect_to root_url(subdomain: false)
      # do not
    end
  end


end

# add farm houses on adstous.com etc when net will be.

#002f34


# 126	Factory & Building
# 127	Factory & Building (Rent)

# 128 Plaza & Mall
# 129 Plaza & Mall rent

#130 farm houses
#131 farm houses (Rent)