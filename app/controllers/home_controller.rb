class HomeController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:contact_us]
  authorize_resource :class => false


  def index
    @page_title = 'Free classifieds in Pakistan, Buy and sell mobile phones, cars, property - Adstous'
    @page_description = 'Adstous, the new web base Ads portal in Pakistan for Buy and Sell goods in all categories mobiles, cars, electronics, motorcycles, bikes, houses, apartments, flats, lands, plots, plot files,
    animals, property, business, furniture, jobs, pets, industrial, agriculture, kids, services, fashion '
  end

  def user_items
    @bred_crumbs = "/"
    if params[:tab] == "posted"
      @items = Item.where("user_id = ?",current_user.id).order('created_at DESC') if current_user.present?
      @posted = "posted"
      @page_title = "Member posted ads - Adstous"
    elsif params[:tab] == "fav"
      @fav = "fav"
      @page_title = "Member favourite ads - Adstous"
      @items = FavouriteItem.where("user_id = ?",current_user.id) if current_user.present?
    else
      @item = Item.find(params[:id])
      if @item.user.role.name == "Admin" || @item.user.role.name == "sub_admin"
        @items = Item.where("phone = ?",@item.phone).order('created_at DESC')
        @name = @item.full_name
      else
        usr_id = @item.user.id
        @items = Item.where("user_id = ?",usr_id).order('created_at DESC')
        @name = User.find(usr_id).try(:profile).try(:name)
      end
      @page_title = "#{@name} posted ads - Adstous"
      @specific = "specific"
    end
  end


  def contact_us
    @page_title = 'Contact us - Adstous'
        @page_description = 'Adstous, the leading web base Ads portal in Pakistan for Buy and Sell goods in all categories mobiles, cars, electronics, motorcycles, bikes, houses, apartments, flats, lands, plots, plot files,
        animals, property, business, furniture, jobs, pets, industrial, agriculture, kids, services, fashion '
    ApplicationMailer.contact_us(params[:user][:email],params[:name],params[:description]).deliver
    redirect_to root_path,:notice => "Your request has been submitted successfully."
  end

  def contact
    @bred_crumbs = "/"
    @page_description = 'Adstous, the leading web base Ads portal in Pakistan for Buy and Sell goods in all categories mobiles, cars, electronics, motorcycles, bikes, houses, apartments, flats, lands, plots, plot files,
        animals, property, business, furniture, jobs, pets, industrial, agriculture, kids, services, fashion '
  end

  def privacy
    @bred_crumbs = "/"
  end

  def terms
    @bred_crumbs = "/"
  end

  def faq
    @bred_crumbs = "/"
    @page_title = 'Frequently asked question - Adstous'
            @page_description = 'Adstous, the leading web base Ads portal in Pakistan for Buy and Sell goods in all categories mobiles, cars, electronics, motorcycles, bikes, houses, apartments, flats, lands, plots, plot files,
            animals, property, business, furniture, jobs, pets, industrial, agriculture, kids, services, fashion '

  end

  def autocomplete_json
    render json: City.where("lower(name) LIKE ? ", "#{params[:query].try(:downcase)}%").order('name').limit(12).map {|p| p.name }
    # render json: City.where("lower(name) LIKE ? ", "#{params[:query].downcase}%").order('name').limit(12).map {|p| p.name+" in province "+p.province.name }
  end



  def autocomplete_json1
    # 13,14 ids is of propertis. will include only in property portal
    # items = Item.select('DISTINCT ON (items.sub_category_id) items.sub_category_id, items.name').where("lower(name) LIKE ? ", "#{params[:query1].try(:downcase)}%").order('sub_category_id,created_at DESC,name').limit(12).map {|p| p.name.scan(/\w+ \w+ \w+/).first+" in categories "+p.sub_category.name }
    items = Item.select('DISTINCT ON (items.sub_category_id) items.sub_category_id, items.name').where("lower(name) LIKE ? and category_id != ? and category_id != ?", "#{params[:query1].try(:downcase)}%",13,14).order('sub_category_id,created_at DESC,name').limit(12).map do |p|
      if p.name.split.size == 1
        p.name.scan(/\w+/).first+" in categories "+p.sub_category.name
      elsif p.name.split.size == 2
        p.name.scan(/\w+ \w+/).first+" in categories "+p.sub_category.name
      else
        p.name.scan(/\w+ \w+ \w+/).first+" in categories "+p.sub_category.name
      end
    end
    if items.present?
      render json: items
    else
      render json: ["No record found"]
    end
      # render json: Item.where("name LIKE ? ", "#{params[:query]}%").order('name').limit(10).map {|p| p.name.split(" ").first+" in categories "+p.sub_category.name }
  end

  def sitemap
    @page_title = 'Free classifieds in Pakistan, Sitemap | Adstous'
        @page_description = 'Adstous, the new web base Ads portal in Pakistan for Buy and Sell goods in all categories mobiles, cars, electronics, motorcycles, bikes, houses, apartments, flats, lands, plots, plot files,
        animals, property, business, furniture, jobs, pets, industrial, agriculture, kids, services, fashion '
    @categories = Category.all
    respond_to do |format|
          format.html
          format.xml
        end

  end

  def admin
    @page_title = '2 easy steps to post free Ad, Free classifieds in Pakistan - Adstous'
        @page_description = 'Adstous, the leading web base Ads portal in Pakistan for Buy and Sell goods in all categories mobiles, cars, electronics, motorcycles, bikes, houses, apartments, flats, lands, plots, plot files,
        animals, property, business, furniture, jobs, pets, industrial, agriculture, kids, services, fashion '
    # @bred_crumbs = '/admin'
    # @items = Item.where(:status => :inactive)
    @items = Item.where("created_at >= ?", Time.zone.now.beginning_of_day)

  end

  def item_active_inactive
    array = params[:item_id].split("-")
    status = array[0]
    id = array[1]
    a = Item.find_by_id(id).update(:status => status)
    render :partial => 'home/kids',:layout => false
  end

  def all_categories

    url_param = request.url.split('/').last
    @bred_crumbs = "/all-categories/#{url_param}"
    if url_param == "mobiles"
      @name = 'Mobiles'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]

    elsif url_param == "electronics-home-appliances"
      @name = 'Electronics & Home Appliances'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
    elsif url_param == "property-for-sale"
      @name = 'Property for Sale'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
    elsif url_param == "property-for-rent"
      @name = 'Property for Rent'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
    elsif url_param == "books-sports-hobbies"
      @name = 'Books, Sports & Hobbies'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
    elsif url_param == "fashion-beauty"
      @name = 'Fashion & Beauty'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
    elsif url_param == "business-industrial-agriculture"
      @name = 'Business, Industrial & Agriculture'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
    elsif url_param == "marriages"
      @name = 'Marriages'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
    elsif url_param == "furniture-home-decor"
      @name = 'Furniture & Home Decor'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
    elsif url_param == "vehicles"
      @name = 'Vehicles'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
    elsif url_param == "bikes"
      @name = 'Bikes'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
    elsif url_param == "services"
      @name = 'Services'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
    elsif url_param == "jobs"
      @name = 'Jobs'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
    elsif url_param == "kids"
      @name = 'Kids'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
    elsif url_param == "animals"
      @name = 'Animals'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
    end

    @page_title = "Free classifieds ads for #{@name} in Pakistan - Adstous"

    @page_description = "Using Adstous Pakistan, you can post classified ads in #{@name}, Post unlimited classified ads in different categories like mobile phones, tablets,cars, electronics goods, bikes, houses, lands, plots, plot files,
            animals, property, business,services, furniture,birds, jobs, pets"
    # query = "SELECT s.name AS NAME,SUM(CASE v.vote_flag WHEN 1 THEN 1 ELSE -1 END) AS software_count FROM apps s ,answers a,questions q,votes v
    #              WHERE q.id = a.question_id
    #              AND s.id = a.app_id AND a.id = v.votable_id  AND q.id = #{@question.id}
    #              GROUP BY s.name ORDER BY software_count DESC LIMIT 20"

    # @users_currently_love_app = FavoriteApp.where("user_id = ? AND is_currently_using = ? AND is_favorite =? ", user.id, true, true).limit(10)
    #      @users_past_love_app = FavoriteApp.where("user_id = ? AND is_past_using = ? AND is_favorite =? ", user.id, true, true).limit(10)
    #      @answers_count = Answer.where("user_id = ?", user.id).count
    #      @post_votes = Vote.where("voter_id = ? AND vote_flag = ?", user.id, true).limit(5).order("id DESC")
    #      @post_questions = Question.where("user_id = ?", user.id).limit(5).order("id DESC")
    #      @post_answers = Answer.where("user_id = ?", user.id).limit(5).order("id DESC")
    #      @question_follow_count = Follow.where("follower_id = ? AND followable_type = ?", user.id, "Question").limit(5).order("id DESC")
    #      @user_follow_count = Follow.where("follower_id = ? AND followable_type = ?", user.id, "User").limit(5).order("id DESC")
    #      @software_follow_count = Follow.where("follower_id = ? AND followable_type = ?", user.id, "App").limit(5).order("id DESC")
    #      @followers_count = user.followers_count
    #      @follow_user = current_user.following?(user) unless current_user.blank?
    #    end
    #  end
    #
    #  def user_answers_vote_up_count(usr)
    #    query = "select count(*)  from answers a , votes v
    #                      where a.id = v.votable_id and v.vote_flag = true and a.user_id = #{usr.id}"
    #    votes_count = DbQuery.execute_data_base_raw_query(query)


    # result = @connection.exec_query(query);

    # binding.pry

  end

  def show_content
    @flag = "flag"
    if params["name"] == "Mobiles"
      @name = params["name"]
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
      render :partial => "home/mobiles_tablets", :layout => false
    elsif params["name"] == "Marriages"
      @name = params["name"]
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
      render :partial => "home/marriages", :layout => false
    elsif params["name"] == "Electronics "
      @name = 'Electronics & Home Appliances'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
      render :partial => "home/electronics", :layout => false
    elsif params["name"] == "Vehicles"
      @name = params["name"]
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
      render :partial => "home/cars", :layout => false
    elsif params["name"] == "Bikes"
      @name = params["name"]
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
      render :partial => "home/bikes", :layout => false
    elsif params["name"] == "Fashion "
      @name = 'Fashion & Beauty'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
      render :partial => "home/fashion_beauty", :layout => false
    elsif params["name"] == "Furniture "
      @name = 'Furniture & Home Decor'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
      render :partial => "home/furniture", :layout => false
    elsif params["name"] == "Animals"
      @name = params["name"]
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
      render :partial => "home/pets", :layout => false
    elsif params["name"] == "Services"
      @name = params["name"]
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
      render :partial => "home/services", :layout => false
    elsif params["name"] == "Jobs"
      @name = params["name"]
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
      render :partial => "home/jobs", :layout => false
    elsif params["name"] == "Books, Sports "
      @name = 'Books, Sports & Hobbies'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
      render :partial => "home/books", :layout => false
    elsif params["name"] == "Business, Industrial "
      @name = 'Business, Industrial & Agriculture'
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
      render :partial => "home/agriculture", :layout => false
    elsif params["name"] == "Kids"
      @name = params["name"]
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
      render :partial => "home/kids", :layout => false
    elsif params["name"] == "Property for Sale"
      @name = params["name"]
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
      render :partial => "home/property_for_sale", :layout => false
    elsif params["name"] == "Property for Rent"
      @name = params["name"]
      two_values = get_all_names_and_count(@name)
      @names = two_values[0]
      @total_count = two_values[1]
      render :partial => "home/property_for_rent", :layout => false
    else
      render json: "No record found"
    end
  end


  private

  def get_all_names_and_count(name)
    category = Category.find_by_name(name)
    sub_categories = category.sub_categories unless category.blank?
    @connection = ActiveRecord::Base.connection
    names_and_count = []
    total_ads_count = 0
    sub_categories.each do |a|
      query = "Select count(*)  as item_counts

                     From items as I

                     Inner Join sub_categories as SC on SC.id = I.sub_category_id

                     Inner join categories as C  on C.id = SC.category_id


                     Where I.sub_category_id = (select id FROM sub_categories WHERE name = '#{a.name}')"
      result = @connection.exec_query(query);

      subarray = []
      subarray << a.name
      subarray << a.slug
      total_ads_count = total_ads_count + result[0]["item_counts"]
      subarray << result[0]["item_counts"]
      subarray << a.id
      names_and_count << subarray  if result[0]["item_counts"] > 0
    end unless sub_categories.blank?
    return names_and_count, total_ads_count
  end


end
