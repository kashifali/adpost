class PhaseSectorsController < ApplicationController

  #TODO currently only give option of add phase sectors. no option for edit

  load_and_authorize_resource
  before_action :set_phase_sector, only: [:show, :edit, :update, :destroy]

  # GET /sub_categories
  # GET /sub_categories.json
  def index
    
    @phase_sectors = PhaseSector.all
    # @phase_sectors = PhaseSector.where("category_id = ? or category_id=?",13,14)
    # @bred_crumbs = "/sub_categories"
  end

  # GET /sub_categories/1
  # GET /sub_categories/1.json
  def show
    # @bred_crumbs = "/sub_categories/#{@phase_sector.slug}"
  end

  def show_societies
        @city = City.find_by_id(params[:city_id])
        @societies = @city.societies unless @city.blank?
        render :partial => 'items/societies', :layout => false
    end

  # GET /sub_categories/new
  def new
    @phase_sector = PhaseSector.new
    @cities = City.all
    # @societies = Society.all
    # @bred_crumbs = "/sub_categories/new"
  end

  # GET /sub_categories/1/edit
  def edit
    @cities = City.all
    # @bred_crumbs = "/sub_categories/#{@phase_sector.slug}/edit"
  end

  # POST /sub_categories
  # POST /sub_categories.json
  def create
    @phase_sector = PhaseSector.new(phase_sector_params)
    #binding.pry
    # pppppppp
    @phase_sector.society_id = params[:phase_sector][:society_id]
    @phase_sector.city_id = params[:phase_sector][:city_id]
    respond_to do |format|
      if @phase_sector.save
        format.html { redirect_to phase_sectors_path, notice: 'phase sector was successfully created.' }
        format.json { render :show, status: :created, location: @phase_sector }
      else
        @cities = City.all
        flash[:error] = []
        @phase_sector.errors.full_messages.each { |error| flash[:error] << error }
        format.html { render :new }
        format.json { render json: @phase_sector.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sub_categories/1
  # PATCH/PUT /sub_categories/1.json
  def update
    respond_to do |format|
      if @phase_sector.update(phase_sector_params)
        format.html { redirect_to phase_sectors_path, notice: 'Sub category was successfully updated.' }
        format.json { render :show, status: :ok, location: @phase_sector }
      else
        # @categories = Category.all
        flash[:error] = []
        @phase_sector.errors.full_messages.each { |error| flash[:error] << error }
        format.html { render :edit }
        format.json { render json: @phase_sector.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sub_categories/1
  # DELETE /sub_categories/1.json
  def destroy
    @phase_sector.destroy
    respond_to do |format|
      format.html { redirect_to sub_categories_url, notice: 'Sub category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_phase_sector
      @phase_sector = PhaseSector.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def phase_sector_params
      params.require(:phase_sector).permit(:name, :society_id,:city_id)
    end
end
