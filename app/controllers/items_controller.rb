class ItemsController < ApplicationController
  load_and_authorize_resource
  before_action :redirect_path , only: [ :new,:account_setting,:change_password]
  before_action :authenticate_user! , only: [ :account_setting,:change_password,:update_password,:new,:create,:edit,:update_profile,:like_dislike,:like_dislike_index]
  before_action :set_item, only: [ :edit, :update]

  # GET /items
  # GET /items.json
  def index
    @items = Item.all
  end

  def account_setting
    @bred_crumbs = "/"
  end

  def change_password
    @bred_crumbs = "/"
  end

  def update_profile
    if current_user.present?
      profile = current_user.profile
      if params[:property].present?
        profile.update(name: params[:name],phone: params[:phone],business_name: params[:business_name],
            office_address: params[:office_address],timings: params[:timings],avatar: params[:avatar],line_number: params[:line_number])
      else
        profile.update(name: params[:name],phone: params[:phone])
      end
      respond_to do |format|
        format.html {redirect_to '/account-setting', notice: 'Your account settings updated.'}
      end
    else
      redirect_to root_path
    end
  end

  def update_password
    @user = current_user
    
    if @user.update(user_params)
      bypass_sign_in(@user)
      respond_to do |format|
        format.html {redirect_to '/change-password', notice: 'Your password has changed.'}
      end
    else
      redirect_to root_path
    end
  end

  # GET /items/1
  # GET /items/1.json
  def show                                              
    @item = Item.friendly.find(params[:id])
    authorize! :read, @item
    @bred_crumbs = "/items/#{@item.slug}"
    @favourite = FavouriteItem.where(:user_id => current_user.try(:id),:item_id => @item.id)
    @page_title = "#{@item.name} - #{@item.sub_category.name} - Adstous"
    @page_description = @item.description
    if @item.views_count.to_i == 1
      @item.update(:views_count => @item.views_count.to_i + 10)
    else
      @item.update(:views_count => @item.views_count.to_i + 1)
    end
  end

  def like_dislike
    @f_item = FavouriteItem.where("item_id = ? and user_id = ? ", params[:item_id] , current_user.id).last if current_user.present?
    @item = Item.find_by_id(params[:item_id])
    if current_user.present? and @f_item.blank?
      @favourite = FavouriteItem.create!(:item_id => params[:item_id] , :user_id => current_user.id)
      render :partial => "items/like_area" ,:layout => false
    elsif @f_item.present?
       @f_item.destroy
       render :partial => "items/like_area" ,:layout => false
    end
  end

  def like_dislike_index
    @f_item = FavouriteItem.where("item_id = ? and user_id = ? ", params[:item_id] , current_user.id).last if current_user.present?
    @item = Item.find_by_id(params[:item_id])
    if current_user.present? and @f_item.blank?
      @favourite = FavouriteItem.create!(:item_id => params[:item_id] , :user_id => current_user.id)
      render :partial => "search/like_area_index" ,:layout => false
    elsif @f_item.present?
       @f_item.destroy
       render :partial => "search/like_area_index" ,:layout => false
    end
  end

  # GET /items/new
  def new
    @bred_crumbs = "/"
    @item = Item.new
    # @categories = Category.all
    @provinces = Province.all
    @page_title = "Post free ads unlimited - Adstous"
    @item_attachment_attachment = @item.item_attachments.build
  end

  # GET /items/1/edit
  def edit
  end

  # POST /items
  # POST /items.json
  def create
    @item = Item.new(item_params)
    sub_category = SubCategory.find_by_name(params[:item][:sub_category_id])
    @item.user = current_user || User.first
    @item.sub_category = sub_category
    @item.category = sub_category.category
    @item.is_featured = 1  if current_user.is_featured == 1
    @item.city_id = params[:city]
    @item.society_id = params[:society]
    @item.phase_sector_id = params[:item][:phase_sector_id]
    @item.block_id = params[:item][:block_id]
    price = @item.get_values(params["price"])
    condition = @item.get_values(params["vehicle_condition"])
    area_unit = @item.get_values(params["area_unit"])
    area = @item.get_values(params["area"])
    @item.price = price.gsub(',','') if price.present?
    @item.area= area.gsub(',','')    if area.present?
    @item.area_unit = area_unit
    @item.facilities = params[:facilities]
    @item.vehicle_condition = condition
    profile = current_user.profile if current_user
    @item.vehicle_drive_in_km = params[:item][:vehicle_drive_in_km].gsub(',','') # if user enter comma in text field
    @item.job_salary_from = params[:item][:job_salary_from].gsub(',','') # if user enter comma in text field
    @item.job_salary_to = params[:item][:job_salary_to].gsub(',','') # if user enter comma in text field
    # binding.pry
    # ppppp
    if profile.present? and current_user.role.name == "user"
      profile.update(phone: params[:phone],city_id: params[:city]) if profile.phone.blank?
    elsif profile.present? and (current_user.role.name == "sub_admin" or current_user.role.name == "Admin")
      @item.phone= params[:phone]
      @item.full_name= params[:full_name]
    end

    respond_to do |format|
      if @item.save
        params[:item_attachments]['avatar'].each do |a|
            @post_attachment = @item.item_attachments.create!(:avatar => a[1])
        end   unless params[:item_attachments].blank?
        # format.html { redirect_to "/#{@item.sub_category.slug}", notice: 'Ad was successfully created.It will appear as soon as approved by our team.' }
        if request.url.include? 'zameen'
          format.html { redirect_to "/property/#{@item.slug}", notice: 'Ad was successfully created.' }
        else
          format.html { redirect_to @item, notice: 'Ad was successfully created.' }
        end
        # format.html { redirect_to root_path, notice: 'Ad was successfully created.It will appear as soon as approved by our team.' }
        format.json { render :show, status: :created, location: @item }
      else
        flash[:error] = []
        @item.errors.full_messages.each { |error| flash[:error] << error }
        # @categories = Category.all
        @provinces = Province.all
        @item_attachment_attachment = @item.item_attachments.build
        format.html { render :new }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
        format.json { render :show, status: :ok, location: @item }
      else
        format.html { render :edit }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to admin_url, notice: 'Item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def show_sub_category

    if  params[:category_name] == "Electronics "
      params[:category_name] = 'Electronics & Home Appliances'
    elsif  params[:category_name] == "Fashion "
      params[:category_name] = 'Fashion & Beauty'
    elsif  params[:category_name] == "Furniture "
      params[:category_name] = 'Furniture & Home Decor'
    elsif  params[:category_name] == "Books, Sports "
      params[:category_name] = 'Books, Sports & Hobbies'
    elsif  params[:category_name] == "Business, Industrial "
      params[:category_name] =  "Business, Industrial & Agriculure"
      name1 = 'Business, Industrial & Agriculture'
    elsif  params[:category_name] == "Marriages"
      @marriage = 1
    end
    @category = Category.find_by_name(params[:category_name]) || Category.find_by_name(name1)
    @sub_categories = @category.sub_categories  unless @category.blank?
    render :partial => 'items/sub_category_select', :layout => false
  end

  def show_cities
    @province = Province.find_by_id(params[:state_id])
    @cities = @province.cities unless @province.blank?
    render :partial => 'items/cities', :layout => false
  end

  def show_societies
    cat = Category.find_by_name(params[:name]) if params[:name].present?
    if cat.present? and cat.name == "Property for Sale" || cat.name == "Property for Rent"
      @city = City.find_by_id(params[:city_id])
      @societies = @city.societies unless @city.blank?
      render :partial => 'items/societies', :layout => false
    elsif params[:phase].present?
      @city = City.find_by_id(params[:city_id])
      @societies = @city.societies unless @city.blank?
      render :partial => 'phase_sectors/societies', :layout => false
    else
      render  :json => "",:layout => false
    end
  end

  def show_sub_category_product
    @sub_category = SubCategory.find_by_name(params[:sub_category_name])
    @products = @sub_category.products  unless @sub_category.blank?
    if @products.present?
      render :partial => 'items/sub_category_product_select', :layout => false
    else
      render :partial => 'items/sub_category_product_select', :layout => false
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    def redirect_path
      redirect_to login_path,alert: "You need to login or register to perform this action" unless current_user.present?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:name, :description,:facilities, :status,:price, :vehicle_model, :vehicle_transaction_type,
                     :vehicle_registered_in, :vehicle_year, :vehicle_fuel_type,:vehicle_drive_in_km, :vehicle_condition, :furnished,
                     :bed_rooms, :bath_rooms, :floor_level,:area_unit, :area, :job_salary_period,
                     :job_salary_from, :job_salary_to, :job_position_type,:user_id,:city_id, :sub_category_id,:category_id,
                     :age_from, :age_to, :height_feet,:height_inch,:country, :cast,:religion, :education,:religious_sect,:ethnic_background,
                     :product_id ,:item_attachments_attributes => [:id, :avatar])
    end


    def user_params
      # NOTE: Using `strong_parameters` gem
      params.require(:user).permit(:password, :password_confirmation)
    end

end


