class PropertyController < ApplicationController

  # authorize_resource :class => false

  has_scope :sub_category_id
  has_scope :city_id
  has_scope :society
  has_scope :property_sale
  has_scope :beds
  has_scope :baths
  # has_scope :min_price
  # has_scope :max_price
  has_scope :min_area
  has_scope :max_area
  has_scope :area_unit

  def index
    @properties =  apply_scopes(Item).all.order('created_at DESC').page(params[:page]).per(12)
  end

  def profile_detail
    @usr = User.find_by_id(params[:id])
    @properties = Item.where("category_id IN (?) and user_id = ?", [13,14],@usr.id).page(params[:page]).order('created_at DESC').per(12) if @usr.present?
  end

  def show
    @featured_users = User.where(:is_featured => true).limit(10)
    @item = Item.friendly.find(params[:id])
        authorize! :read, @item
        @bred_crumbs = "/items/#{@item.slug}"
        @favourite = FavouriteItem.where(:user_id => current_user.try(:id),:item_id => @item.id)
        @page_title = "#{@item.name} - #{@item.sub_category.name} - Adstous"
        @page_description = @item.description
        if @item.views_count.to_i == 1
          @item.update(:views_count => @item.views_count.to_i + 10)
        else
          @item.update(:views_count => @item.views_count.to_i + 1)
        end

  end


  def autocomplete_society
    if params[:slug].present?
      c = City.find_by_id(params[:slug])
      societies = Society.where("lower(name) LIKE ? and city_id =? ", "#{params[:query3].try(:downcase) || params[:query4].try(:downcase) }%",c.id).order('name').limit(12).map {|p| p.name }
    else
      societies = Society.where("lower(name) LIKE ? ", "#{params[:query3].try(:downcase) || params[:query4].try(:downcase) }%").order('name').limit(12).map {|p| p.name }
    end

    if societies.present?
      render json: societies
    else
      render json: ["No record found"]
    end

  end

  def property_for_sale
    sale_rent_property
  end


  def property_for_rent
    sale_rent_property
  end



  def list_city_property_sale
    sale_rent_city_property
  end

  def list_city_property_rent
    sale_rent_city_property
  end

  def list_city_society_property_sale
    sale_rent_city_society_property
  end

  def list_city_society_property_rent
    sale_rent_city_society_property
  end


  private

  def sale_rent_city_society_property
    @featured_users = User.where(:is_featured => true).limit(10)
    # by default housese tab and id we are sent as display tab

    if params[:page].blank?
      @slug = request.url.split('/').last.split('?').first
    else
      @slug =request.url.split('/').last.split('?').first
    end
    society = Society.find_by_slug(@slug)
    # society = Society.find_by_id(65)
    ids = get_category
    id = ids[0]
    connection = ActiveRecord::Base.connection

    city_society_flats_houses_count_query = "select sc.name,sc.id,count(*) as count from items as I

                                        Inner Join sub_categories as sc on sc.id = I.sub_category_id

                                        Inner Join societies as so on so.id = I.society_id

                                        Where I.society_id IS NOT NULL and sc.category_id = '#{id}' and so.city_id = '#{society.city_id}' and so.id = '#{society.id}'

                                        group by sc.name,sc.id"

    query = "select I.id from items as I

                   Inner Join cities as c on c.id = I.city_id

                   Inner Join societies as so on so.id = I.society_id

                   Inner Join sub_categories as sc on sc.id = I.sub_category_id

                   Where I.society_id IS NOT NULL and sc.category_id = '#{id}' and I.sub_category_id = #{params[:p]} and so.city_id = '#{society.city_id}'

                   and I.society_id = '#{society.id}' "

    items = connection.exec_query(query);
    @property_and_count = connection.exec_query(city_society_flats_houses_count_query);

    ids  = items.map {|a| a['id']}

    if Rails.env.production?
      @url_exact = request.url.split(request.domain)[1].split('?')[0] #for localhost
    else
      @url_exact = request.url.split(request.domain+':3000')[1].split('?')[0] #for localhost
    end
    @properties = Item.where("id IN (?)", ids).page(params[:page]).order('created_at DESC').per(12) if ids.present?
  end

  def sale_rent_property
    @featured_users = User.where(:is_featured => true).limit(10)
        ids = get_category
        id = ids[0]
        # binding.pry
        connection = ActiveRecord::Base.connection




        all_flats_houses_count_query = "select sc.name,sc.id,count(*) as count from items as I

                                                    Inner Join sub_categories as sc on sc.id = I.sub_category_id

                                                    Inner Join societies as so on so.id = I.society_id

             Where I.society_id IS NOT NULL and sc.category_id = 13

                                                    group by sc.name,sc.id"


        query = "select c.name,c.slug,count(*) as city_count from items as I

                                 Inner Join cities as c on c.id = I.city_id

                                 Inner Join sub_categories as sc on sc.id = I.sub_category_id

                                 Where I.society_id IS NOT NULL and sc.category_id = '#{id}'

                                 group by c.name,c.slug"
        @cities_and_count = connection.exec_query(query);
        ids = SubCategory.where(:category_id => id).map {|a| a['id']} if id.present?
        @properties = Item.where("sub_category_id IN (?)", ids).page(params[:page]).order('created_at DESC').per(12) if ids.present?
  end

  def sale_rent_city_property
    @featured_users = User.where(:is_featured => true).limit(10)
    if params[:page].blank?
      @city_slug = request.url.split('/').last
    else
      @city_slug =request.url.split('/').last.split('?').first
    end
    city_id = City.find_by_slug(@city_slug).try(:id)
    ids = get_category
    id = ids[0]
    @p = ids[1]
    connection = ActiveRecord::Base.connection

    city_flats_houses_count_query = "select sc.name,sc.id,count(*) as count from items as I

                                            Inner Join sub_categories as sc on sc.id = I.sub_category_id

                                            Inner Join societies as so on so.id = I.society_id

     Where I.society_id IS NOT NULL and sc.category_id = 13 and so.city_id = 171

                                            group by sc.name,sc.id"


    query = "select so.name,so.slug,count(*) as society_count from items as I

                                    Inner Join cities as c on c.id = I.city_id

                                    Inner Join societies as so on so.id = I.society_id

                                    Inner Join sub_categories as sc on sc.id = I.sub_category_id

                                    Where I.society_id IS NOT NULL and sc.category_id = '#{id}' and so.city_id = '#{city_id}'

                                    group by so.name,so.slug"
    @cities_and_count = connection.exec_query(query);
    ids = SubCategory.where(:category_id => id).map {|a| a['id']} if id.present?
    @properties = Item.where("sub_category_id IN (?) and city_id = ?", ids,city_id).page(params[:page]).order('created_at DESC').per(12) if ids.present?
    # binding.pry
  end

end
