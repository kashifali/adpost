class BlocksController < ApplicationController

  #TODO currently only give option of add phase sectors. no option for edit

  load_and_authorize_resource
  before_action :set_block, only: [:show, :edit, :update, :destroy]

  # GET /sub_categories
  # GET /sub_categories.json
  def index
    
    @blocks = Block.all
    # @blocks = Block.where("category_id = ? or category_id=?",13,14)
    # @bred_crumbs = "/sub_categories"
  end

  # GET /sub_categories/1
  # GET /sub_categories/1.json
  def show
    # @bred_crumbs = "/sub_categories/#{@block.slug}"
  end

  def show_phases
        @s = Society.find_by_id(params[:society_id])
        @sectors = @s.phase_sectors unless @s.blank?
        # authorize! :read, @sectors
        if params[:item].present?
          render :partial => 'items/phases', :layout => false
        else
          render :partial => 'blocks/phases', :layout => false
        end
    end

  def show_blocks
         @p = PhaseSector.find_by_id(params[:phase_id])
         @blocks = @p.blocks unless @p.blank?
         render :partial => 'items/blocks', :layout => false
  end


  # GET /sub_categories/new
  def new
    @block = Block.new
    @cities = City.all
    # @societies = Society.all
    # @bred_crumbs = "/sub_categories/new"
  end

  # GET /sub_categories/1/edit
  def edit
    @cities = City.all
    # @bred_crumbs = "/sub_categories/#{@block.slug}/edit"
  end

  # POST /sub_categories
  # POST /sub_categories.json
  def create
    @block = Block.new(block_params)

    @block.society_id = params[:phase_sector][:society_id]
    @block.city_id = params[:block][:city_id]
    @block.phase_sector_id = params[:block][:phase_sector_id]
    respond_to do |format|
      if @block.save
        format.html { redirect_to blocks_path, notice: 'Block was successfully created.' }
        format.json { render :show, status: :created, location: @block }
      else
        @cities = City.all
        flash[:error] = []
        @block.errors.full_messages.each { |error| flash[:error] << error }
        format.html { render :new }
        format.json { render json: @block.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sub_categories/1
  # PATCH/PUT /sub_categories/1.json
  def update
    respond_to do |format|
      if @block.update(block_params)
        format.html { redirect_to blocks_path, notice: 'Block was successfully updated.' }
        format.json { render :show, status: :ok, location: @block }
      else
        # @categories = Category.all
        flash[:error] = []
        @block.errors.full_messages.each { |error| flash[:error] << error }
        format.html { render :edit }
        format.json { render json: @block.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sub_categories/1
  # DELETE /sub_categories/1.json
  def destroy
    @block.destroy
    respond_to do |format|
      format.html { redirect_to sub_categories_url, notice: 'Block was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_block
      @block = Block.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def block_params
      params.require(:block).permit(:name, :society_id,:city_id,:phase_sector_id)
    end
end
