class AvatarUploader < CarrierWave::Uploader::Base
  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  # include CarrierWave::MiniMagick
  # include CarrierWave::ImageOptimizer
  include Cloudinary::CarrierWave





  # Choose what kind of storage to use for this uploader:
  # storage :file
  # storage :fog

  # def public_id
  #   binding.pry
    # return model.item.name
  # end


  # cloudinary_transformation :transformation => [
  #   {:width => 633, :height => 400, :crop => :limit},
  #   {:opacity => 90, :color => "rgb:00BFFF", :overlay=>{:font_family=>"Vollkorn", :font_weight => "bold",:font_size=>80, :text=>"ADSTOUS"}},
  #   {:quality => 50}
  #   ]

  # cloudinary_transformation :transformation => [
  # {:width => 495, :height => 420, :crop => :limit},
  # {:opacity => 40, :color => "rgb:1D292F", :overlay=>{:font_family=>"Vollkorn",:font_size=>20, :text=>"********ADSTOUS********"}},
  # {:quality => 90}
  # ]

  cloudinary_transformation :transformation => [
    {:width => 495, :height => 420, :crop => :limit},
    {:opacity => 100, :color => "rgb:DCDCDC", :overlay=>{:font_family=>"Vollkorn",:font_size=>15, :text=>"⚎⚎⚎ ᾸDSTOUS ⚍⚍⚍"},:gravity=>"north",y: 50},
    {:opacity => 100, :color => "rgb:DCDCDC", :overlay=>{:font_family=>"Vollkorn",:font_size=>15, :text=>"⚎⚎⚎ ᾸDSTOUS ⚍⚍⚍"}, :gravity=>"center"},
    {:opacity => 100, :color => "rgb:DCDCDC", :overlay=>{:font_family=>"Vollkorn",:font_size=>15, :text=>"⚎⚎⚎ ᾸDSTOUS ⚍⚍⚍"}, :gravity=>"center",y: 100},
    {:quality => 90}
  ]







  # version :simple do
  #   process :resize_to_fill => [633, 400,:north]
  #   process :convert => 'jpg'

    # cloudinary_transformation :transformation => [
    #     {:width => 633, :height => 400, :crop => :limit},
    #     {:overlay => "my_watermark", :width => 30, :gravity => :south_east,
    #      :x => 5, :y => 5}
    #   ]


    # cloudinary_transformation :quality => 50
  # end


  # end

  # Generate a 100x150 face-detection based thumbnail,
  # round corners with a 20-pixel radius and increase brightness by 30%.
  # version :bright_face do
  #   cloudinary_transformation :effect => "brightness:30", :radius => 20,
  #     :width => 100, :height => 150, :crop => :thumb, :gravity => :face
  # end









  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:

  # def store_dir
  #   "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  # end



  # process resize_to_fit: [380, 350]

  # version :thumbnail do
  #   process optimize: [{ quality: 50 }]
  # end

###################################### watermark code
    # process resize_to_fill: [633,400]
    # process optimize: [{ quality: 50 }]

    # process :watermark    # sorry not working with cloudinary

  def watermark
    # binding.pry

    # manipulate! do |img|
    #   logo = Magick::Image.read(Rails.root.join("lib", "assets", "banner.png")).first
    #   img = img.composite(logo, Magick::NorthWestGravity, 15, 0, Magick::OverCompositeOp)
    # end

    watermark = "#{Rails.root}/lib/assets/banner.png"
    manipulate! do |img|
      img = img.composite(MiniMagick::Image.open(watermark), "png") do |c|
        # c.compose "Over"
        c.gravity "SouthWest"
        c.geometry "+80+90+20+20" # copy second_image onto first_image from (20, 20)

        # binding.pry
        # c.draw 'Over 0,0 0,0'
        # bbbbbbbbbb
      end

      # img.draw 'image Over 0,0 0,0 "{}"'

    end


  end


end
