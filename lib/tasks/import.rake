namespace :import do

task :ali => :environment do


  city_lhr = City.find_by_name("Lahore")

  ['Abdalian Housing Society Phase 1',
   'Abid City',
   'Abid City 2',
   'Abid Town',
   'Abouzar Housing Scheme',
   'Abpara Housing Scheme',
   'Accounts Group Housing Scheme',
   'AGRICS Town',
   'AGRICS Town Phase 2',
   'AGRICS Town Phase 3',
   'Ahbab Cooperative Housing Scheme',
   'Ahmed Housing Scheme',
   'Ahsan Park',
   'Air Lines Housing Scheme',
   'Aitchison College Employees Cooperative Housing Scheme',
   'Akhtar Estate 1',
   'Ark Villas',
   'Al-Rehmat Housing Scheme',
   'Al-Asar Housing Scheme',
   'Al-Hamd Forts',
   'Al-Hamra Town',
   'Ali Town',
   'Alia Town',
   'AlJanat Housing Scheme',
   'Almawa Housing Scheme',
   'Alpha Cooperative Housing Society',
   'Angoori Bagh Scheme',
   'Architects Engineers Housing Scheme Phase 1',
   'Architects Engineers Housing Scheme Phase 2',
   'Army Welfare Trust Housing Scheme Phase 1',
   'Army Welfare Trust Housing Scheme Phase 2',
   'Askari 1',
   'Askari 2' ,
   'Askari 3'  ,
   'Askari 5'   ,
   'Askari 8'    ,
   'Askari 9'     ,
   'Askari 10'     ,
   'Askari 11'      ,
   'Audit & Accounts Housing Societ',
   'Avecinia City',
   'Awaisia Housing Scheme',
   'Ayubia Town',
   'Azam Garden Housing Scheme',
   'Azizia Town',
   'Bagh-e-Iram Housing Scheme',
   'Bahria Nasheman',
   'Bahria Orchard',
   'Bahria Town',
   'Bankers Avenue Cooperative Housing Scheme',
   'Beacon House',
   'Campus Colony',
   'Campus View Town',
   'Canal Burg',
   'Cantt Lahore',
   'Canal View Cooperative Housing Scheme',
   'Central Park',
   'China Town',
   'Chinar Bagh Housing Scheme',
   'Chinar Courts Farm Housing Scheme',
   'Chodary Khushi Muhammad',
   'City Park Housing Scheme',
   'City Star Residency',
   'Class IV Employees Cooperative Housing Scheme',
   'DHA(Defence Housing Authority)',
   'DHA(EME)',
   'DHA 11 Rahbar',
   'DHA City',
   'Dilkusha Colony',
   'Dream Gardens',
   'Dream Gardens Phase 2',
   'Dubai Town',
   'Land Housing Schemehman',
   'Eden Abad',
   'Eden Abad Extension',
   'Eden Abad Ext-1(A+B)',
   'Eden Boulevard',
   'Eden Canal Villas Housing Scheme',
   'Eden Gardens',
   'Eden Gardens Extension',
   'Eden Lane Villas',
   'Eden Park',
   'Eden Place Housing Scheme',
   'Education City',
   'EME Co-operative Housing Scheme',
   'Engineering University Employees Cooperative Housing Scheme',
   'Engineers & Architects Housing Scheme Phase 3',
   'Etihad Town',
   'Ever Green Housing Scheme',
   'Excise – Taxation',
   'Farooq Avenue',
   'Fazaia Housing Scheme',
   'Fazal Elahi Colony',
   'Formanites Housing Scheme Phase 1',
   'General Sher Ali Khan Housing Scheme',
   'Ghause Farm',
   'Ghee Corporation of Pakistan',
   'Ghousia Town',
   'Gardiono Courts Housing Scheme',
   'GM Riaz',
   'Gosha-e-Ahbab Phase 2',
   'Gosha-e-Ahbab Phase 3',
   'Gosha-e-Ahbab Co-operative Housing Scheme',
   'Government Superior Services',
   'Government Officers Cooperative Housing Scheme (C & D Sector)',
   'Government Servant Housing Scheme OLD',
   'Green Acres Farm Housing Scheme',
   'Green Forts 4',
   'Green Forts 1',
   'Green Valley',
   'Green Forts 2 (Rev)',
   'Guldasht Town',
   'Gulfishan Town',
   'Gulshan-e-Ahbab Housing Scheme Phase 2 (Shadab Colony)',
   'Gulshan-e-Awan Housing Scheme',
   'Gulshan-e-Dawood Housing Scheme',
   'Gulshan-e-Jinnah Housing Scheme',
   'Gulshan-e-Lahore Housing Scheme',
   'Gulshan-e-Rail Housing Scheme Phase 2',
   'Gulshan-e-Mustafa Housing Scheme',
   'Gulshan-e-Shalimar Housing Scheme',
   'Hassan Town',
   'Home Land(Eden Palace Villas)',
   'Hyde Park',
   'Icon Valley',
   'IEP Town Sector B',
   'IEP Town Sector A',
   'International City Farm Housing Scheme',
   'Iqbal Avenue Cooperative Housing Scheme Phase 1',
   'Iqbal Avenue Phase 3',
   'Irrigation Society',
   'Islamia College Old Boys Housing Scheme',
   'Ittefaq Town',
   'Izmir Town',
   'Johar Town',
   'Jaffar Town/Ismat Nagar',
   'Jewan Mal',
   'Judicial Employees Cooperative Housing Scheme Phase 1',
   'Judicial Employees Cooperative Housing Scheme Phase 1 Extension',
   'Judicial Colony Phase 2',
   'Judicial Housing Scheme Phase 3',
   'Judicial Cooperative Housing Scheme Phase 3 Extension',
   'Kakezai Phase 2',
   'KEMC (Doctors town)',
   'Khayaban-e-Quaid',
   'Khayam Mini City Phase 1',
   'Khuram Town',
   'Khayaban-e-Amin',
   'Khayaban-e-Khair-ud-Din',
   'Khayaban-e-Khair-ud-Din (Ext)',
   'Khayaban-e-Zahra',
   'Khyber Housing State',
   'Kings Town Housing Scheme',
   'Lahore Avenue',
   'Lahore Chamber of Commerce Housing Scheme Phase 1',
   'Lahore Chamber of Commerce Housing Scheme Phase 2',
   'Lake City Bella Vista',
   'Land Bareze Sector A',
   'Land Bareze Sector B',
   'Marium Town',
   'Mascot Housing Scheme',
   'Meraj Town Housing Scheme',
   'Mid Land Farm Housing Scheme',
   'Millat Tractors Employees Housing Scheme',
   'Model Housing Scheme',
   'Muhafiz Town Phase 1',
   'Muhafiz Town phase 2',
   'Muslim Nager',
   'Naddi Kinaray',
   'Nasheman-e-Iqbal Cooperative Housing Scheme Phase 1',
   'Nasheman-e-Iqbal Cooperative Housing Scheme Phase 2',
   'Nawab Town',
   'Naz Town',
   'NESPAK Employees Cooperative Housing Scheme Phase 2 Site 1',
   'NESPAK Employees Cooperative Housing Scheme',
   'NESPAK Employees Cooperative Housing Scheme Phase 2',
   'New Lahore City',
   'News Paper Employees Cooperative Housing Scheme',
   'NFC Housing Scheme Phase 1',
   'Nishan Colony',
   'OPF Housing Scheme',
   'P&D Employees Housing Scheme',
   'Passco Employees Cooperative Housing Scheme',
   'Pakistan Atomic Energy Housing Scheme',
   'Pakistan Rajput Town',
   'Pak Arab Housing Scheme',
   'PCSIR Employees Cooperative Housing Society Phase 1',
   'PCSIR ECHS (Alfalah Avenue-1)',
   'PCSIR Employees Cooperative Housing Society Phase 2 Revised',
   'PCSIR Employees Cooperative Housing Society Phase 2 and 3',
   'PGEHS Phase 1 Extension',
   'PIA Housing Scheme Phase 1 & 2',
   'Pindi Raj Putan Housing Scheme',
   'Public Health Engineering Dept. Employees Cooperative Housing Scheme',
   'Punjab Board of Revenue Housing Scheme',
   'Punjab Cooperative Housing Scheme',
   'Punjab Government Employees Housing Scheme Phase 1',
   'Punjab Government Employees Housing Scheme Phase 1 Extension',
   'Punjab Government Employees Housing Scheme Phase 2',
   'Punjab University Employees Housing Scheme Phase 1',
   'Punjab University Employees Housing Scheme Phase 2',
   'Real Cottages',
   'Rehan Garden',
   'Rehman City - Phase 4',
   'Rehman Housing Society',
   'Rehman Park',
   'Rehman Villas',
   'Rail Town',
   'Ravians Housing Scheme',
   'Raza Town Housing Scheme',
   'Regent Park',
   'Rehman Abad',
   'Revenue Employees Cooperative Housing Scheme Block A',
   'Revenue Employees Cooperative Housing Scheme Block B',
   'River Edge Housing Scheme (Revised)',
   'River View Cooperative Housing Scheme',
   'Royal Residencia Housing Scheme',
   'Saddat Cooperative Housing Scheme',
   'Saighal Estate Housing Scheme',
   'Sally Town North',
   'Sally Town South',
   'Saman Berg Khurd',
   'Saman Burg',
   'Saman Zar',
   'Sargodha Cooperative Housing Scheme',
   'Saroba Garden',
   'Satellite Town',
   'Shadab Colony',
   'Shahid Town',
   'Sheraz Housing Scheme',
   'State Enterprises Housing Scheme',
   'State Enterprises Cooperative Housing Scheme Phase 1',
   'State Enterprises Cooperative Housing Scheme Phase 2',
   'Sui Northern Gas Employees Cooperative Housing Scheme Phase 1',
   'Sui Northern Officers Housing Scheme Phase 2',
   'Sukh Chayn Garden',
   'Sun Flower Housing Scheme (Revised)',
   'Sunny Park Housing scheme',
   'Sunny Park Housing Scheme (Extension)',
   'Superior Courts Employees Housing Scheme',
   'Tariq Gardens Housing Scheme',
   'TECH. Society (The Engineers Cooperative Housing Scheme)',
   'Tricon Village Housing Scheme',
   'Tuxedo Farm Housing Scheme',
   'Valencia Housing Scheme Phase 1-4',
   'Valencia Housing Scheme Phase 5',
   'Venus Housing Scheme',
   'Wapda Employees Cooperative Housing Scheme Phase 1',
   'Wapda Employees Cooperative Housing Scheme Phase 1 Extension',
   'Wapda Retired Officers Housing Scheme',
   'Wapda Town',
   'Wapda Town phase 2',
   'West Wood Colony',
   'Women Housing Scheme',
   'Wyeth Employees Cooperative Housing Scheme',
   'Zaheer-ud-Din Babar',
   'Zubaida Khatoon',
   'Other Society in Lahore'].each do |s|
    Society.create(name: s,city: city_lhr)
  end

  str ="Abdullah Garden
Agha Shahi Avenue
Agro Farming Scheme
Ahmed Town
Airline Avenue
Airport Enclave
Aiza Garden
Akbar Town
Al Huda Town
Al Madina City
Al Qaim Town
Al-Kabir Town
Alhamra Avenue
Ammar Enclave And Talha Villas
Arcadia City
Arsalan Town
Ataturk Avenue
12th Avenue
7th Avenue
9th Avenue
Atomic Energy Employee Society
Axis Mall & Apartments
Azmat Town
B-17
Bahria Town
Burma Town
C-14
C-15
C-16
C-17
C-18
C-19
Capital Enclave
CBR Town
Central Avenue
Cantt Islamabad
City Garden
Commander Enclave
Commerce Town
Commoners Flower Valley
Commoners Gold Valley
Commoners Sky Gardens
Constitution Avenue
D-12
D-13
D-14
D-16
D-17
D-18
Danial Town
Darussalam Coop Society
DHA Defence Islamabad
Diplomatic Enclave
E-10
E-11
E-12
E-13
E-14
E-15
E-16
E-17
E-18
E-19
E-6
E-7
E-8
Eden Life Islamabad
Emaar Canyon Views
Engineering Co-operative Housing (ECHS)
F-10
F-11
F-12
F-13
F-14
F-15
F-16
F-17
F-5
F-6
F-7
F-8
F-9
Faisal Town - F-18
Fatima Town
FECHS
Federal Government Employees Housing Foundation
Fenarina Residences
FOECHS - Foreign Office Employees Society
Frash Town
G-10
G-11
G-12
G-13
G-14
G-15
G-16
G-17
G-5
G-6
G-7
G-8
G-9
Gandhara City
Garden Town
Ghauri Town
Grace Valley
Graceland Housing
Green Avenue
Green City Islamabad
Green Hills Housing Scheme
Green Huts Farmhouses
Gulshan-e-Khudadad
Gulshan-e-Rabia
H-10
H-11
H-12
H-13
H-15
H-17
H-19
H-8
H-9
I-10
I-11
I-12
I-13
I-14
I-15
I-16
I-17
I-8
I-9
Icon Garden
Iqbal Town Islamabad
Islamabad Enclave
Islamabad Garden
Islamabad View Valley
Ittefaq Town
J and K Zone 5
Jeddah Town
Jinnah Avenue
Judicial Town
Kahuta Triangle Industrial Area
Karakoram Diplomatic Enclave
Karakoram Enclave 1
Kashmir Highway
Kashmir Town
Khayaban-e-Iqbal
Khayaban-e-Suhrwardy
Koral Town
Korang Town
Kuri Model Town
Lawyers Society
Madina Town
Margalla Town
Margalla Valley - C-12
Marwa Town
Meherban Colony
Ministry of Commerce Society
Motorway City
Mubasher Homes
Multi Residencia & Orchards
Mumtaz City
NARC Colony
National Cooperative Housing Society
National Town
Naval Anchorage
Naval Farms Housing Scheme
Naval Housing Scheme
New Airport Town
NHA Housing Society
NIH Colony
OGDC Officers Cooperative Housing Society
OGDC Town
OPF Valley
Orchard Scheme
PAEC Employees Cooperative Housing Society
PAF Tarnol
Pakistan Intelligence Bureau Housing Society
Pakistan Town
Park Enclave
Park View City Islamabad
Partal Town
PECHS
Police Foundation Housing Society
PTV Colony
PWD Housing Scheme
Qutbal Town
Rawal Dam Colony
Rawal Enclave
Rawal Town
River Garden
Royal Avenue
Royal City
Sehab Gardens
Sehala Farm House
Senate Secretariat Employees Cooperative Housing Society
Shaheen Town
Shalimar Town
Shehzad Town
Sihala Valley
Silver City Housing Scheme
Soan Garden
Sohan Valley
Spring Valley
State Life Insurance Employees Cooperative Housing Society
Swan Garden
The Organic Farms Islamabad
Top City 1
University Town
Victoria Heights
Wapda Town Islamabad
Zaraj Housing Scheme
Zero Point
Zone 5
Other Society in Islamabad"
  city_isl = City.find_by_name("Islamabad Capital")
  str.split(/\n+/).each do |s|
    Society.create(name: s,city: city_isl)
  end

  str_k ="10009*Abid Town
      9950*Ahsan Grand City
      11941*Al-Hilal Society
      11358*Al-Jadeed Residency
      10260*Al-Manzar Society
      12501*Al-Safaa Garden
      9215*Ali Garh Society
      6563*Allama Iqbal Town Karachi
      11638*Amil Colony
      12845*APP Employees Co-operative Housing Society
      10599*ASF Housing Scheme
      11892*ASF Tower
      10733*Azam Town
      8539*Bagh-e-Korangi
      8298*Bahria Town Karachi
      1063*Baldia Town
      8962*Baloch Colony
      11208*Bangladesh Colony
      198*Bath Island
      10335*Bilal Town
      677*Bin Qasim Town
      9734*Blue Bell Residency
      10555*BMCHS
      12954*Callachi Cooperative Housing Society
      11251*Catholic Colony
      9714*Chapal Courtyard
      9720*Chapal Uptown
      6668*Comissioner Coop Housing Society
      11599*Cosmopolitan Society
      9069*Cotton Export Cooperative Housing Society
      11195*Cutchi Memon Cooperative Housing Society
      9780*Dastgir Colony
      10685*Defence Garden
      8274*Defence View Society
      6669*Delhi Colony
      1429*DHA City Karachi
      213*DHA Defence Karachi
      10417*Diamond City
      11691*Erum Villas
      9142*Etawa Society
      9700*Falaknaz Presidency
      11088*Falcon Complex Faisal
      9135*Fazaia Housing Scheme
      12*Federal B Area
      12325*Fine City
      224*Frere Town
      10759*Friends Royal City
      570*Gadap Town
      1289*Gobal Town
      6813*Gulberg Town
      232*Gulistan-e-Jauhar
      11939*Gulshan-e-Amin
      11968*Gulshan-e-Areesha
      12231*Gulshan-E-Faisal
      6858*Gulshan-e-Iqbal Town
      12843*Gulshan-e-Jami
      12424*Gulshan-e-Karim
      11193*Gulshan-e-Malir
      1310*Gulshan-e-Mehmood ul Haq
      10064*Gulshan-e-Millat
      11956*Gulshan-e-Mustafa
      1184*Gulshan-e-Tauheed
      12226*Gulshan-e-Umair
      10092*Gulshan-e-Usman Housing Society
      9070*Gulshan_e_Ghazian
      12114*Guru Mandir Chorangi
      9046*Hawks Bay Scheme 42
      6914*Hazara Colony
      758*Hill Park
      12842*Humaira Town
      6915*Intelligence Colony
      6916*Jamshed Town
      8297*Javed Bahria Coopretive Housing Society
      9059*Jinnah Avenue
      12045*Jinnah Residency
      11354*Jodhpur Society
      10124*Karachi Golf City
      10905*Kashmir Colony
      8292*KDA Scheme 1
      6957*Kemari Town
      9974*Khudadad Colony
      9537*KPT Officers Society
      11022*Laraib Garden
      9204*Liaquat Avenue
      7104*Lyari Expressway
      488*Lyari Town
      10155*Madina City Housing Scheme
      1222*Malir Cantt Karachi
      6605*Malir Housing Scheme 1
      10918*Malir Link To Super Highway
      9854*Maqboolabad Society
      11639*Muhammad Bin Qasim Co-operative Housing Society
      12425*Muslimabad Society
      9198*Naval Housing Scheme
      11579*Navy Housing Scheme Karsaz
      10079*Naya Nazimabad
      7143*Neelam Colony
      1304*New Karachi
      9045*Nooriabad Industrial Area
      282*North Karachi
      11*North Nazimabad
      7224*NTR Colony
      12637*Numaish Chowrangi
      9052*Old Clifton
      9149*Old Ravians Co-Operative Housing Society
      682*Orangi Town
      11233*P & T Colony
      9110*PAF Housing Scheme
      10614*Pak Ideal Cooperative Housing Society
      11764*Peoples Colony
      1380*PIDC
      12337*Police Society
      11077*PTV Society
      7253*Punjab Colony
      10667*Rabia City
      12169*Raji City
      9739*Royal Appartment
      9743*Royal Defence Tower
      11013*Rufi Lake Drive Apartments
      9973*Rufi Rose Petals
      683*S.I.T.E
      7269*Saddar Town
      9748*Saharanpur Cooperative Housing Society
      10136*Saima Luxury Homes
      11616*Sarfaraz Colony
      495*Scheme 33
      1026*Scheme 45
      7292*Sea View Apartments
      9642*Seven Wonders City
      8959*Shadman 2
      7309*Shah Rasool Colony
      9419*Shah Town
      9963*Sindh Industrial Trading Estate
      11656*State Bank of Pakistan Staff Co-Operative Housing Society
      11846*Surti Muslim Co-Operative Housing Society
      11709*Tahir Villa
      9073*Teacher Society
      9072*Teachers Society
      11651*Techno City
      457*Timber Merchants Co-operative Society
      10108*Times Residency
      12122*Volks Cooperative Housing Society Limited
      416*Zamzama
  3*Other Society in Karachi"

  city_k = City.find_by_name("Karachi")
                str_k.split(/\n+/).each do |s|
                  Society.create(name: s.split('*').last,city: city_k)
                end

  str_p = "5984*Affandi Colony
        1387*Afshan Colony
        1127*Air Force Housing Society
        426*Airport Housing Society
        8818*Al-Haram City
        10643*Al-Noor Colony
        11348*Ali Town
        1325*Allama Iqbal Colony
        5986*Ameen Town
        5987*ARL Colony
        5988*Army Officers Colony
        5989*Ashraf Colony
        1294*Asia Housing Scheme
        9438*Askari 1 Rawalpindi
        1516*Askari 10 Rawalpindi
        443*Askari 11 Rawalpindi
        5990*Askari 12 Rawalpindi
        1515*Askari 13 Rawalpindi
        1504*Askari 14 Rawalpindi
        10475*Askari 15 Rawalpindi
        9439*Askari 2 Rawalpindi
        8156*Askari 3 Rawalpindi
        9214*Askari 4 Rawalpindi
        9440*Askari 5 Rawalpindi
        1270*Askari 7 Rawalpindi
        9441*Askari 8 Rawalpindi
        1492*Askari 9 Rawalpindi
        5991*Askari Villas Rawalpindi
        10642*Aslam Colony
        8964*Awan Town
        1037*AWT Housing Scheme
        10641*Ayub Colony
        721*Ayub National Park
        5992*Azeem Town
        5995*Badar Colony
        10348*Bahar Colony
        632*Bahria Town Rawalpindi
        11076*Bangash Colony
        5999*Bankers Colony
        6000*Behari Colony
        6001*Bethsaida Colony
        12001*Bilal Colony
        12241*Bostan Valley
        12615*Cantt Rawalpindi
        12615*Capital Awami Villas
        12502*Capital Housing Society
        11891*Capital Smart City
        5980*Chaklala Scheme
        6002*Chaudhary Jan Colony
        6004*Christian Colony
        9488*City Town
        8877*City Villas
        1300*Defence Colony
        10071*Doctors Housing Society
        11103*Eastridge Housing Scheme
        6012*Faisal Colony
        516*Fazaia Colony
        1382*Fazal Town
        6016*Friends Colony
        1339*Ghauri Town
        6018*Ghousia Colony
        10427*Gulbahar Scheme
        739*Gulistan Colony
        11487*Gulistan Fatima Colony
        6019*Gulistan-e-Jinnah Colony
        438*Gulraiz Housing Scheme
        12168*Gulshan-e-Bahar
        6020*Gulshan-e-Iqbal
        1501*Gulshan-e-Kashmir
        10946*Gulshan-e-Saeed
        12956*Gulshan-e-Safeer
        10520*Gulshan-e-Shamal
        11017*Gulshan-e-Zaheer Colony
        540*Gulzar-e-Quaid Housing Society
        6021*Hazara Colony
        11811*Ideal Homes Society
        10755*Imran Khan Avenue
        9852*Islamabad Farm Houses
        6023*Ittehad Colony Rawalpindi
        9290*Janjua Town
        1520*Jinnah Colony
        1266*Judicial Colony
        1423*Kashmir Model Town
        6025*Khayaban-e-Faisal
        587*Khayaban-e-Sir Syed
        10817*Khayaban-e-Tanveer
        6026*Khurram Colony
        13078*Kingdom Valley Islamabad
        9814*Kohsar Town
        6027*Lalarukh Colony
        714*Liaquat Bagh
        10947*Liaquat Colony
        6028*Magistrate Colony
        11690*Malik Colony
        11139*Mangral Town
        8157*Media Town
        6030*Millat Colony
        12373*MMB City Housing Scheme
        11853*Model Colony
        10489*Model Town Humak
        6033*Mohammadi Colony
        8282*Mumtaz Colony
        6034*Munawar Colony
        9013*National Garden Housing Scheme
        11240*Nawaz Colony
        10883*New Afzal Town
        8920*New Airport Town
        6037*Nussah Town
        711*Ojhary Camp
        463*Old Punjab House
        6038*PAF Residential Area
        10887*Palm City
        12514*Park View Rawalpindi
        10349*Peer Meher Ali Shah Town
        11096*People Colony
        9165*PIA Colony
        11209*PIA Cooperative Officers Housing Society
        1271*Police Foundation Housing Scheme
        6039*Professors Colony
        10424*Punjab Government Servant Housing Foundation (PGSHF)
        348*PWD Colony
        6040*Quaid-e-Azam Colony
        1475*Qurtaba City
        9012*Rail View Housing Society
        12698*Railway Scheme 7
        12923*Railway Scheme 9
        11269*Rawat Enclave
        9051*Rawat Industrial Estate
        1435*RCCI
        8293*Safari View Residencia
        9179*Samarzar Housing Society
        6041*Sangar Town
        433*Satellite Town
        12863*Shadman Town
        6045*Shah Khalid Colony
        6046*Shaheen Town
        1404*Shalley Valley
        8854*Sher Zaman Colony
        12259*Sir Syed Avenue
        6050*Supreme Court Employees Housing Society
        6051*T & T Colony
        11491*Taj Residencia
        11752*UpCountry Enclosures Housing Society
        722*Walait Homes
        10350*Wazir Town
        6057*Wireless Residential Colony
        6058*Yousaf Colony
        6059*Zeeshan Colony
        6060*Zulfiqar Colony
        6060*Other Society in Rawalpindi"

  city_p = City.find_by_name("Rawalpindi")
              str_p.split(/\n+/).each do |s|
                Society.create(name: s.split('*').last,city: city_p)
              end

  str_f ="  4889*Abdullah City
    11937*Abdullah Defence Housing Scheme
    11938*Abdullah Farm Housing Scheme
    4900*Air Avenue City
    4901*Akbar Colony
    9527*Al Barkat Villas
    8833*Al Fayaz Colony
    11371*Al Jameel Housing Scheme
    12139*Al Mehboob Garden
    4902*Al Najaf Colony
    8916*Al Noor Garden
    9856*Al Rehmat Villas
    12467*Al-Harmain City
    9832*Al-Qadir Garden
    10400*Ali Garden
    4903*Ali Housing Colony
    4904*Ali Town
    4905*Allama Iqbal Colony
    10379*Allied Housing Scheme
    1362*Amin Town
    10389*Amir Town
    8544*Aqsa Town
    4907*Ayub Colony
    11801*Bara City
    11608*Barra City
    8588*Canal Garden
    10820*Canal Park
    10454*Canal View Hosuing Scheme
    10454*Cantt Faisalabad
    8358*Citi Housing Society
    4930*D Type Colony
    12217*Daniyal Town
    8589*Defence Fort Housing Scheme
    12429*Dilawar Colony
    9931*Dry Port City
    8296*Eden Gardens
    11162*Eden Orchard
    8917*Eden Valley
    9831*Faisal City
    4934*Faisal Gardens
    1361*Faisal Town
    9830*Faisal Valley
    957*Fareed Town
    4936*Farid Town
    371*FDA City
    4944*Firdous Colony
    4945*Four Season Housing
    4946*Freed Colony
    538*Garden Colony
    10300*Garden Town
    11821*Ghafoor Gardens Housing Scheme
    10165*Ghalib City
    8821*Green City
    1388*Green Town
    9140*Green View Colony
    4948*Gulbahar Colony
    10823*Gulberg Valley
    4950*Gulfishan Colony
    11517*Gulgasht Colony
    1073*Gulistan Colony No 1
    1680*Gulistan Colony No 2
    9927*Gulshan Colony
    1252*Gulshan e Madina
    10418*Gulshan Rehman
    11606*Gulshan-e-Ali Housing Scheme
    10397*Gulshan-e-Aziz
    8545*Gulshan-e-Habib
    9443*Gulshan-e-Hameed
    10378*Gulshan-e-Haram
    11955*Gulshan-e-Hayat
    4954*Gulshan-e-Rafique
    11189*Gulshan-e-Rehman
    4955*Gulzar Colony
    4957*Hajveeri Town
    4958*Hanif Garden
    4959*Harchan Pura
    743*Haseeb Shaheed Colony
    9787*Hassan Villas
    4962*Ideal Colony
    9776*Ideal Town
    12466*Industrial Area Faisalabad
    4964*International Housing Society
    9829*Iqbal Garden
    639*Iqbal Town
    4965*Islamia Park
    11200*Ismail City
    12617*Ismail Green Valley
    4966*Ismail Town
    10709*Ismail Valley
    4967*Jameel Town
    640*Jaranwala Town
    9407*Jinnah Colony
    641*Jinnah Town
    1077*Johar Colony
    4969*Judicial Employees Coop Housing Society
    4971*Kaleem Shaheed Colony No 1
    4972*Kaleem Shaheed Colony No 2
    9452*Kareem City
    9526*Kareem Garden
    646*Karim Town
    4975*Kehkishan Colony No 1
    4976*Kehkishan Colony No 2
    4977*Kehkishan Colony No 3
    4979*Khalid Garden
    11935*Khalid Town
    4980*Khayaban Colony
    9475*Khayaban Colony 2
    4981*Khayaban Gardens
    11518*Khayaban-e-Greens
    4982*Khayaban-e-Kareem
    648*Khurrianwala Town
    9775*Kiran Villas
    1360*Kohinoor City
    10313*Kohinoor Town
    4984*Lasani Garden
    420*Lasani Town
    9835*Latif Garden
    11065*Lawyers Housing Society
    4985*Liaquat Town
    11468*Lyallpur Garden
    642*Lyallpur Town
    11803*Lyllapur Villas
    8547*Madina Gardens
    13205*Madina Green Valley
    533*Madina Town
    10821*Makkah Garden
    11671*Mannan Town
    13128*Mediacom Avenue
    4991*Metropool Colony
    645*Millat Town
    1022*Model Town
    1254*Motorway City
    11261*Muhammad Ali Housing Scheme
    1156*Muslim Town
    5008*Muzaffar Colony
    9833*Naveed Garden
    12871*Nawab Town
    11936*Nazimabad Villas
    5013*Nemat Colony No 1
    5014*Nemat Colony No 2
    10521*New Garden Town
    11154*New Green Town
    1128*Nisar Colony
    8550*Officers Colony
    5016*Paradise Valley
    535*Peoples Colony No 1
    1681*Peoples Colony No 2
    8548*Punjab Govt. Servants Housing Foundation
    5025*Punjab Small Industries Estate
    5026*Qamar Garden
    10406*Quaid-e-Azam Park
    5028*Rabbani Colony
    5029*Rachna Town
    5030*Raheem Gardens
    5031*Railway Colony
    1078*Raja Colony
    5032*Raja Park
    5035*Rasool Park
    11802*Ravi Gardens
    1068*Raza Garden
    5037*Rehman Gardens
    5039*Rehman Town
    10394*Rehman Villas
    5040*Rehmania Town
    9928*Royal Garden
    582*Saeed Colony
    643*Samundari Town
    5046*Satellite Town
    5048*Shadab Colony
    5049*Shadman Colony
    5050*Shahbaz Town
    9454*Shahzad Colony
    5051*Shakeel Park
    1355*Shalimar Park
    5052*Sheikh Colony
    5053*Siddiqia Mill Colony
    5054*Sir Syed Town
    5055*Sitara Colony
    11131*Sitara Qamar Villas
    1211*Sitara Sapna City
    8549*Sitara Supreme City
    1367*Sitara Valley
    12152*SMD Homes
    5056*Steam Power Station Colony
    644*Tandlianwala Town
    11236*The Meadows Phase 1
    9331*Umair Town
    5062*Umar Town
    10514*Umer Garden
    9836*Umer Valley
    5063*University Town
    8970*Usman Town
    5065*Valencia Gardens
    12143*Vista Homes
    9834*Waheed Garden
    501*Wapda City
    5079*Wapda Town Faisalabad
    5084*Yasir Town
    11499*Yasrab Colony
    5085*Younas Town
    8921*Yousaf Town
    9444*Zeenat Town
    1081*Zia Colony
    5086*Zubair Colony
    6060*Other Society in Faisalabad"

  city_f = City.find_by_name("Faisalabad")
            str_f.split(/\n+/).each do |s|
              Society.create(name: s.split('*').last,city: city_f)
            end

  str_b = "12261*Akbar Colony
    9623*Al Aqsa Commercial Centre
    12766*Al-Hadi Garden
    9773*Al-Shaheer Garden
    9366*Allama Iqbal Town
    9337*Arif Town
    11232*Bahawalpur Avenue Housing Society
    10906*Bankers Colony
    703*Bano Colony
    9378*Behari Colony
    11064*Canal Colony
    11064*Cantt Bahawalpur
    12476*Canal Garden Housing Scheme
    692*Chaudhary Town
    697*Cheema Town
    694*Commercial Colony
    4667*Darbar Mahal Town
    9420*DHA Defence Bahawalpur
    12304*Dilawar Colony
    9335*Faisal Bagh Town
    4671*Garden Town
    11060*Goheer Town
    4672*Government Employees Cooperative Housing Society
    12825*Government Servants Housing Scheme
    9340*Green Town
    693*Gulistan Colony
    695*Hamaitian Awami Colony
    11576*Hamza Town
    10903*Haroon Town
    11461*Haseeb Town
    9857*Imtiaz Town
    9532*Iqbal Villas
    702*Islamia Colony
    11068*Jafer Colony
    690*Jillani Colony
    12341*Johar Town
    699*Khakwani Colony
    8525*Khayaban-e-Ali Housing Society
    11442*Khursheed Town
    10133*Madni Avenue Housing Scheme
    4685*Maqbool Colony
    700*Medical Colony
    13261*Meer Garden Housing Scheme
    12343*Middle City
    12460*Millat Colony
    8421*Model Avenue
    581*Model Town A
    4691*Model Town B
    8230*Model Town C
    10443*Muhammadia Colony
    9401*Mushtaq Colony
    1313*Muslim Town
    701*Nazir Abad Colony
    9336*New Satellite Town
    9338*Nishat Colony
    9379*One Unit Chowk
    12257*Paragon City Bahawalpur
    13036*Paragon Ideal Homes
    10355*Paragon Villas Bahawalpur
    9631*Pelican Homes
    1281*Qasim Town
    13252*Rafaqat Town
    9400*Rehmat Colony
    9341*Riaz Colony
    9625*Riaz ul Jannah Society
    10224*Royal City Housing Scheme
    691*Sadiq Colony
    1090*Sajid Awan Colony
    4700*Satellite Extension
    655*Satellite Town
    9624*Satluj Valley Housing Scheme
    4702*Shadab Colony
    9376*Shadman City
    11281*Shahbaz Town
    10860*Trust Colony
    9339*University Chowk
    9528*Zaman Villas
    6060*Other Society in Bahawalpur"

  city_b = City.find_by_name("Bahawalpur")
          str_b.split(/\n+/).each do |s|
            Society.create(name: s.split('*').last,city: city_b)
          end


  str_m = "  922*Abbas Colony
    895*Abid Colony
    5686*Afzal City
    8614*Agric Town
    5687*Ahmad Park
    1091*Ahsan Colony
    898*Akhtar Colony
    8615*Al Falah Modern City
    5689*Al Habib Colony
    5690*Al Jilan Town
    5691*Al Mustafa Colony
    5692*Al Quresh Housing Scheme
    9405*Al Raheem Colony
    666*Al Rehman Colony
    5693*Ali Town
    5694*Altaf Town
    1092*Ameen Town
    8616*Ansar Colony
    919*Ashraf Colony
    5695*Askari Colony
    8617*Askari Colony 2
    5699*Azam City
    8619*Badla Town
    5701*Bhutta Colony
    5702*Bhutta Colony No 2
    5703*Bilal Colony
    5704*Bodla Town
    660*Bosan Town
    8627*Buch Executive Villas
    5706*Bukhari Colony
    8716*BZU Colony
    5707*BZU Employers Colony
    5708*Canal Bank Road
    5709*Canal Cantt View Housing Society
    9477*Canal Cantt Villas
    8629*Cantt Avenue
    8629*Cantt Multan
    5714*Cantt Cottages
    8717*Chenab Boulevard
    8633*China Town
    5720*Circuit House Colony
    12544*Citi Housing
    11824*Crystal Homes
    8581*DHA Defence Multan
    8903*Dream Garden
    5721*Faiz Town
    8643*Farid Town
    667*Fatima Jinnah Town
    9026*Fazaia Town
    12123*Fida Avenue
    9414*Fort Avenue
    5734*Fort Colony
    8644*Furrukh Town
    9409*Galaxy Town
    8645*Garden Citi
    924*Garden Town
    5735*Gardezi Colony
    5737*Gawala Colony
    5738*Grace Gardens
    5739*Green Fort Housing Scheme
    5740*Green Fort Plus
    11919*Green Hearts Colony
    8307*Green Homes
    12741*Green Huts
    10914*Green View Colony
    8648*Gulberg Colony
    664*Gulgasht Colony
    9912*Gulistan Colony
    5741*Gulistan Housing Scheme
    9402*Gulistan-e-Ali Housing Scheme
    5742*Gulistan-e-Zuhra
    5743*Gulraiz Town
    12442*Gulshan-e-Bashir
    5744*Gulshan-e-Faiz
    9413*Gulshan-E-Farooq Pura
    5745*Gulshan-e-Iqbal
    9793*Gulshan-e-Madina
    8649*Gulshan-e-Mehar
    5746*Gulshan-e-Rehman
    8650*Gulshan-e-Toheed
    5747*Gulshan-e-Wahid
    8651*Gulshan-e-Yousaf
    899*Hassanabad Colony
    5748*Hayat Town
    5749*Ibrahim Town
    5750*Icon Villas
    5751*Income Tax Officers Colony
    1149*Industrial Estate Multan
    5752*Iqra Town
    8659*Jamilabad Housing Scheme
    5757*Jan Mohammad Colony
    11440*Jinnah Town
    5758*Johar Town
    5759*Justic Colony
    5760*Justic Hamid Colony
    5761*Khan Colony
    1094*Khan Village
    8664*Khan Village II
    906*Khawaja Farid Colony
    5763*Khayaban-e-Kubra
    5764*La Salle Colony
    5765*Lalazar Colony
    5766*Lasani Colony
    1071*Lodhi Colony
    1403*Madina Town
    9653*Mall OF Multan
    8669*Mall Plaza
    1400*MDA Co-operative Housing Scheme
    3103*MDA Officers Colony
    927*Meherban Colony
    5770*MEO Colony
    5771*MEPCO Colony
    1660*Mid Land Avenue
    1147*Model Town
    5776*Mujahid City
    1399*Mujahid Town
    12002*Mukhtar Town
    8674*Musa Park
    11327*Muzaffargarh Road
    11301*N Gulgasht Boulevard
    1067*Naqshband Colony
    1075*Nasheman Colony
    5780*Nayab City
    5781*Nazir Colony
    5782*New Shah Shams Colony
    12124*New Shalimar Colony
    8679*New Town
    12768*Niaz Town
    5783*Noor-e-Islam Colony
    5784*Officers Canal Colony
    925*Officers Colony
    8681*Officers Town
    5785*Pace City
    8682*Pak Arab Fertilizer Housing Colony
    9412*Pearl City
    918*Peer Colony
    9408*Peer Khurshed Colony
    900*Peoples Colony
    1502*PIA Housing Society
    8685*Prime Villas
    5786*PSIC Employees Housing Scheme
    5787*PTCL Colony
    5788*Punjab Govt Servants Housing Foundation Scheme
    1719*Punjab Small Industries Multan
    8686*Qasim Villas
    5790*Qasimpur Colony
    8687*Quaid-e-Azam Cooperative Housing Socity
    5791*Railway Officers Bungalows
    8689*Rana Homes
    5792*Rasheed Avenue
    5793*Raza Town
    9406*Rehmat Town
    9728*Royal Residency
    1074*Sabzazar Colony
    5794*Sadat Colony
    5795*Sadiq Colony
    5796*Sahara Homes
    665*Sakhi Sultan Colony
    8696*Samanabad Colony
    5797*Sarwar Town
    8968*Satellite Town
    5798*Sayyam City
    5799*Sayyam Officers City
    5800*Shadab Colony
    921*Shadman Colony
    5801*Shah Khuram Colony
    658*Shah Rukn-e-Alam Colony
    12263*Shah Town
    11628*Shahzad Colony
    346*Shalimar Colony
    896*Shamasabad Colony
    659*Shershah Town
    8700*Silver City
    8701*Sunshine Housing
    5811*Tahir Colony
    5813*Tawakal Town
    5815*Tughlaq Town
    8705*United Mall
    5816*Usman Colony
    5818*Wahdat Colony
    8706*Walled City
    897*Wapda Colony
    5820*Wapda Town Multan
    8707*Wapda Town Multan Phase II
    9459*Waqas Town
    12872*Western Fort Colony
    902*Writer Colony
    8708*Zaid Town
    1518*Zakariya Town
    8709*Zubair Colony
    6060*Other Society in Multan"

  city_m = City.find_by_name("Multan")
        str_m.split(/\n+/).each do |s|
          Society.create(name: s.split('*').last,city: city_m)
        end


  str_guj ="  5087*Abdullah Colony
    5088*Abid Colony
    5089*Abubakar Park
    5090*Abubakar Town
    863*Afan Gee Town
    5091*Agriculture Colony
    814*Akram Colony
    5092*Al Badar Town
    5093*Al Hafiz Town
    5094*Al Mujeeb Town
    5095*Al Mustafa Town
    5096*Ali Hasan Colony
    809*Allah Buksh Colony
    5097*Allah Rakha Town
    8840*Allama Iqbal Town
    804*Arfaat Colony
    5098*Arif Colony
    5099*Asad Colony
    852*Asghar Colony
    887*Asif Colony
    11816*Aziza Housing Scheme
    815*Batala Town
    5106*Batth Colony 62
    807*Behari Colony
    5114*Bilal Town
    5115*Bismillah Colony
    786*Butt Colony
    1126*Canal View Housing Scheme
    5127*Chenab Park
    5127*Cantt Gujranwala
    5129*Christan Colony
    3083*Citi Housing Society
    8594*Country Homes
    865*Darabha Colony
    871*Data Ganj Baksh Colony
    5121*DHA Defence Gujranwala
    791*Ehtisham Colony
    5140*Faisal Colony
    5141*Faisal Town Gujranwala
    789*Faiz Alam Town
    797*Faqeed Town
    5143*Farid Town
    5144*Fazal Town
    5147*G Magnolia Park
    1056*Garden Town Gujranwala
    5156*Ghousia Colony
    5158*Ghulam Mohammad Town
    11548*Global City Gujranwala
    1326*Green Town Gujranwala
    9133*Gulberg Colony Gujranwala
    5162*Gulberg Town Gujranwala
    5163*Gulistan Colony
    5164*Gulshan Aziz Colony
    824*Gulshan Colony Gujranwala
    818*Gulshan Iqbal Park Gujranwala
    5165*Gulshan Town Gujranwala
    5166*Gulshan-e-Iqbal Town
    5167*Gulshan-e-Iyaz Town
    787*Gulzar Colony
    5176*Haider Colony
    5177*Hajweri Town Gujranwala
    860*Hashim Colony
    5178*Highway Staff Colony
    5179*Hydri Colony
    5180*Iftikhar Colony
    5181*Illyas Colony
    5182*Imran Colony
    821*Industrial Estate Gujranwala
    858*Industrial Estate Gujranwala 1
    869*Industrial Estate Gujranwala 2
    5183*Ittifaq Colony Gujranwala
    798*Ittihad Colony Gujranwala
    5184*Jalal Town Gujranwala
    1493*Jalil Town
    856*Jinnah Colony
    10493*Johar Town Gujranwala
    867*Judicial Housing Colony
    5186*Kalim Colony
    5187*Kamran Colony
    5190*Kashif Park
    5191*Kashmir Colony No 1
    5192*Kashmir Colony No 2
    876*Khalid Colony
    793*Lalk Zar Colony
    795*Madina Colony Gujranwala
    836*Main City Market
    816*Majid Colony
    5200*Malik Park
    9180*Master City Housing Scheme
    5201*Mehar Colony
    5203*Mian Mir Town Gujranwala
    5204*Miran-Je Town
    5206*Moazzam Colony
    825*Model Town Gujranwala
    862*Mohammadia Colony
    5211*Mubarik Colony
    5212*Mufti Colony
    585*Muhafiz Town
    5214*Mukhadar Colony
    5215*Mukhtar Colony
    857*Mumtaz Colony
    843*Muslim Town Gujranwala
    5218*Muzamil Colony
    799*Naveed Town
    845*Nawab Park
    5219*Nazir Colony
    5220*Nazir Park
    5221*New Muslim Colony
    5225*Nusrat Colony
    9111*Palm City Housing Scheme
    5226*Park Royal Town
    5227*Pasban Colony
    805*Peoples Colony
    5234*Popular Nursery Town
    5235*Professors Colony
    5236*Qiaser Colony
    9745*Quaid-e-Azam Town
    1295*Rahwali Cantt
    5238*Raja Colony
    5239*Rajput Town
    853*Rana Colony
    880*Rasheed Colony
    855*Rehman Colony
    5244*Riaz Colony
    796*Saddat Colony
    5245*Sadiq Colony
    5246*Salam Colony
    5247*Sardar Colony
    5248*Sardar Town
    859*Sarfraz Colony
    808*Satellite Town
    1220*Satluj Block
    861*Shadman Colony
    5254*Shadman Town
    890*Shahrukh Colony
    5261*Shalimar Town
    812*Shamasabad
    11134*Sharjah City
    803*Shehzada Shaheed Colony
    5265*Siddique Colony
    833*Siddque Akber Town
    5266*Sulaiman Town
    5267*Tanveer Town
    5268*Umar Farooq Colony
    8848*University Town Housing Scheme
    785*Usman Colony
    5270*Usman Park
    9518*Wafi Citi Housing Scheme
    802*Wahdat Colony
    5271*Waheed Colony
    9638*Wapda City Gujranwala
    430*Wapda Town Gujranwala
    822*Zahid Colony
    6060*Other Society in Gujranwala"

  city_guj = City.find_by_name("Gujranwala")
      str_guj.split(/\n+/).each do |s|
        Society.create(name: s.split('*').last,city: city_guj)
      end

  str_gwdr = "  7945*Akara
    11500*Akbar Industrial Park Gwadar
    10767*Al Shams City
    733*Azhani
    12707*Balochistan Broadway Phase 4
    12529*Balochistan Employees Cooperative Housing Society (BECHS)
    728*Bandi
    1054*Broadway Residencia
    8835*Burj Al Gwadar
    500*Cantt
    7946*Central Boulevard
    731*Chatani Ball
    11418*Chatti Road
    13066*Chib Rikani
    11185*China Village Gwadar
    11184*China Village Jinnah Avenue
    11437*Coastal Highway Avenue
    462*Creek City
    11441*Crown City
    1244*Dismount Residency
    13023*Fish Harbour Road
    11449*Florida City
    10088*FTBA
    1351*GDA
    10081*GDA Housing Scheme No. 5
    12193*GDA Industrial Scheme 3
    7947*GDA Residential Area
    11186*Global Village Gwadar
    12280*Golden Lake Gwadar
    1079*Golden Palms
    7948*Gurab
    9445*Gwadar Industrial Estate
    11299*Highway Castles Farm Housing Society
    1186*Janubi Mouza Ankra
    7949*Jawar Khan
    7950*Jinnah Avenue
    11289*Jinnah Avenue 2
    1187*Jinnah Town
    10508*Jiwani
    7951*Koh e Batail
    7952*Koh e Mehdi
    7953*Langove Abad
    7954*Main Boulevard
    1188*Makka City
    391*Makran Coastal Highway
    7955*Marine Dr
    10713*Mouza Ankara Janobi
    1189*Mouza Ankara Shomali
    1190*Mouza Bandri
    745*Mouza Chatti Janobi
    732*Mouza Chatti Shumali
    726*Mouza Chib Kalmati
    12715*Mouza Chukain
    12714*Mouza Churbander
    1192*Mouza Derbela Janubi
    10515*Mouza Derbela Shumali
    730*Mouza Dhore Ghatti
    1193*Mouza Dubar
    12664*Mouza Gamaro
    1195*Mouza Gunz
    12716*Mouza Gurani
    1196*Mouza Gurundani Janobi
    10714*Mouza Jorkan
    12101*Mouza Kalmat
    1197*Mouza Kappar
    1198*Mouza Karwat
    1199*Mouza Kia Kalat
    9860*Mouza Mazzani
    10944*Mouza Naland
    9861*Mouza Nigor Sharif
    12102*Mouza Paleri
    1200*Mouza Pishukan
    1201*Mouza Pnwan
    1202*Mouza Shabi
    10640*Mouza Shadi Kaur
    724*Mouza Shanikani Dar
    10943*Mouza Shatangi
    11428*Mouza Shumali Bandhan
    7961*Mouza Surbandar
    1203*Mouza Tunk
    729*Mouza Washin Door
    1204*Mouza Zabad Dun
    1205*Mouza Ziarat Machhi Gharbi
    10715*Mouza Ziarat Machhi Sharqi
    8350*New Town
    10535*North Avenue
    11922*Ormara
    390*Others
    744*Passo
    725*Pishukan
    7956*Port Road
    734*Prahin Touk
    12352*Robar
    7957*Sabzi Mandi Road
    735*Sahil Housing Scheme
    7959*Sangar Housing Scheme
    9936*Savaira City Gwadar
    11258*Sea View Housing Scheme
    1206*Seaside Coast
    7958*Shambe Ismail
    7960*Singhar Road
    10540*Sun Silver City
    727*Sur
    12077*West Bay Town
    7962*Yasirs Base Camp
    1207*Zero Point
    6060*Other Society in Gwadar"

  city_gwa = City.find_by_name("Gwadar")
    str_gwdr.split(/\n+/).each do |s|
      Society.create(name: s.split('*').last,city: city_gwa)
    end

  str_pes = "7575*Academy Town
    13192*Afghan Colony
    7579*Akbar Colony
    9044*Al Haram Model Town
    9813*Al-Haram Green
    7580*Albadar Town
    10457*Ali Model Town
    405*Army Housing Scheme
    7581*Ashrafia Colony
    7582*Askari 11 Peshawar
    12121*Askari 2 Peshawar
    8564*Askari 5 Peshawar
    9275*Askari 6 Peshawar
    986*AWT Housing Scheme Badabair
    7585*Bilor Town
    7586*Canal Town Peshawar
    579*Cantt Peshwar
    392*Defence Officer Colony
    9050*DHA Defence Peshawar
    10382*Executive Lodges Arbab Sabz Ali Khan Town
    990*Falcon Complex
    989*Fazaia Housing Scheme
    7591*Fida Abad Colony
    11402*Green Homes
    7594*Gulshan Rehman Colony
    393*Gulshan-e-Iqbal
    7595*Haji Town
    10403*Hussain Abad Colony
    12974*Industrial Estate Peshawar
    12014*Inner City
    7642*Khanmast Colony
    13286*Khyber Colony 1
    13287*Khyber Colony 2
    7644*Lala Rukh Colony
    7646*Madina Colony Peshawar
    7647*Madina Town Peshawar
    994*Mardan Housing Society
    7651*Momin Town Peshawar
    12087*Muslim City
    7652*Nasir Bagh
    12091*NBP (Afshan) Colony
    12164*New City Homes
    8905*OPF Housing Scheme
    396*PAF Peshawar
    12319*Peshawar Garden
    7657*POF Colony
    12359*Police Colony
    1292*Professor Colony
    996*Rasheed Town
    7660*Regi Model Town
    7663*Sathee Town
    7665*Shaheen Town
    12533*Shams-ul-Qamar Town
    7667*Sher Ali Town
    7668*Shinwari Town
    1317*Sikandar Town
    7670*Small Industries Estate
    12303*Tehsil Park
    1437*University Town
    9220*Walled City
    398*Wapda Town Peshawar
    7675*Wazir Colony
    1038*Zaryab Colony
    7681*Zulfiqar Town
    6060*Other Society in Peshawar"

  city_pes = City.find_by_name("Peshawar")
  str_pes.split(/\n+/).each do |s|
    Society.create(name: s.split('*').last,city: city_pes)
  end




  end



end
