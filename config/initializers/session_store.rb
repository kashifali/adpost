if Rails.env.production?
    Rails.application.config.session_store :cookie_store,
                      key: '_app_name_session', domain: ".adstous.com"
else
    Rails.application.config.session_store :cookie_store,
                      key: '_app_name_session', domain: '.lvh.me'
end
