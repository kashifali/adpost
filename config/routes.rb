Rails.application.routes.draw do

  resources :products
  resources :sub_categories
  resources :phase_sectors
  resources :blocks do
    collection do
      get :show_phases
      get :show_blocks
    end
  end

  resources :categories
  resources :items do
    collection do
      get :show_sub_category
      get :show_sub_category_product
      get :show_cities
      get :show_societies
      get :like_dislike
      get :like_dislike_index
      post :update_profile
      post :update_password
    end
  end
  resources :profiles
  resources :home do
    collection do
      get :show_content
      get :autocomplete_json
      get :autocomplete_json1
      get :autocomplete_society
      get :item_active_inactive
      get :user_items
      post :contact_us
      post :update_profile1
    end
  end

  resources :property do
    collection do
      get :autocomplete_society
    end
  end

  resources :search do
    collection do
      # get :
      get :search_property
      post :search
    end
  end


  devise_for :users , :controllers => {
      :omniauth_callbacks => "users/omniauth_callbacks",
      :registrations => "users/registrations",
      :sessions => "users/sessions",
      :passwords => "users/passwords"
  }

  #devise_for :users
  # as :user do

  #get 'admin' => 'users/sessions#new', :as => :new_user_session
  #post 'admin' => 'users/sessions#create', :as => :user_session
  #get "subscriber", :to => "users/sessions#new"
  # get "sign_up", :to => "users/registrations#new"
  #get "sign_out", :to => "users/sessions#destroy"
  # end


  devise_scope :user do
    get 'login', :to => 'users/sessions#new'
    get '/register', :to => "users/registrations#new"
    get "logout", :to => "users/sessions#destroy"
  end

    get "/all-categories/:slug" => "home#all_categories"

  get '/agent/profile',:to => "property#profile_detail"
  get '/faq',:to => "home#faq"
  get '/contact-us',:to => "home#contact"
  get '/account-setting',:to => "items#account_setting"
  get '/change-password',:to => "items#change_password"
  get '/sitemap',:to => "home#sitemap"
  get '/admin',:to => "home#admin"
  get '/about-us',:to => "home#about"
  get '/privacy-policy',:to => "home#privacy"
  get '/terms-conditions',:to => "home#terms"

  # constraints(subdomain: 'zameen') do
    # get '/', to: 'home#index'
  # if request.url.include? "property-sale"
    get '/property-sale', to: 'property#property_for_sale' ,:via => :get
    get '/property-rent', to: 'property#property_for_rent' ,:via => :get

   # ["lahore","gujranwala","gwadar","peshawar","bahawalpur","karachi","faisalabad","islamabad-capital"].each do |list|
     get "property-sale/:slug" => "property#list_city_property_sale"
     get "property-rent/:slug" => "property#list_city_property_rent"
   # end

     get "/:slug" => "search#index"

     get "/property/:slug" => "items#show"

     get "property-sale/:slug1/:slug" => "property#list_city_society_property_sale"
     get "property-rent/:slug1/:slug" => "property#list_city_society_property_rent"
   # end

  # match '/' => 'items#show', :constraints => { :subdomain => /.+/ }


  root :to => "home#index"

  # if Rails.env.production?
  #    get '404', to: 'application#page_not_found'
  #    get '422', to: 'application#server_error'
  #    get '500', to: 'application#server_error'
  # end

  # get '/404', to: 'errors#not_found'
  # get '/500', to: 'errors#internal_server_error'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
