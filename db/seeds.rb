# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)



######################################################### Azad kashmir




azad = Province.create(name: "Azad Kashmir" )

['Bagh' ,
'Bhimber',
'Haveli'  ,
'Hattian Bala' ,
'Kotli'         ,
'Mirpur'         ,
'Muzaffarabad'    ,
'Neelam Valley'    ,
'Palandri'          ,
'Rawalakot'          ,
'Sudhanoti'].each do |a|

  City.create(name: a, province: azad)

end


########################################################### Balochistan

blochistan = Province.create(name: "Balochistan" )

['Bela',
'Chaman',
'Dera Murad Jamali',
'Dera Allah Yar'    ,
'Gwadar'             ,
'Hub'                 ,
'Kalat'                ,
'Kharan'                ,
'Khuzdar'                ,
'Loralai'                 ,
'Mastung'                  ,
'Nushki'                    ,
'Pasni'                      ,
'Quetta'                      ,
'Sibi'                         ,
'Turbat'                        ,
'Usta Mohammad'                  ,
'Zhob'].each do |b|

  City.create(name: b,province: blochistan)

end


##############################################################################Gilgit

gilgit = Province.create(name: "Gilgit Baltistan" )

['Askole',
'Astore'  ,
'Bunji'    ,
'Chilas'    ,
'Chillinji'  ,
'Chiran'      ,
'Gakuch'       ,
'Ghangche'      ,
'Ghizer'         ,
'Gilgit'          ,
'Dayor'            ,
'Jalalabad'         ,
'Jutial Gilgit'      ,
'Hunza'               ,
'Gorikot'              ,
'Gulmit'                ,
'Jaglot'                 ,
'Chalt'                   ,
'Thole'                    ,
'Nasir Abad'                ,
'Mayoon'                     ,
'Khana Abad'                  ,
'Hussain Abad'                 ,
'Qasimabad Masoot'              ,
'Nagar Proper'                   ,
'Ghulmat'                         ,
'Karimabad',
'Ishkoman' ,
'Khaplu'   ,
'Minimerg' ,
'Misgar'   ,
'Passu'    ,
'Shimshal' ,
'Skardu'   ,
'Sust'     ,
'Thowar'   ,
'Kharmang' ,
'Roundo'].each do |g|

  City.create(name: g,province: gilgit)

end



###########################################################Islamabad

islamabad = Province.create(name: "Islamabad" )
City.create(name: "Islamabad Capital",province: islamabad  )

######################################################################################## FATA

fata= Province.create(name: "FATA" )

['Alizai'  ,
'Bara'      ,
'Darra Adam Khel' ,
'Ghulam Khan'      ,
'Jamrud'            ,
'Khar'               ,
'Landi Kotal'         ,
'Makeen'               ,
'Mir Ali'               ,
'Miranshah'              ,
'Parachinar'              ,
'Razmak'                   ,
'Sadda'                     ,
'Wana'].each do |f|

  City.create(name: f,province: fata  )

end

########################################################################  KPK

kpk= Province.create(name: "Khyber Pakhtunkhwa" )



['Abbottabad' ,
#Adezai
#Alpuri
'Akora Khattak',
'Ayubia'        ,
'Banda Daud Shah',
'Bannu'           ,
'Batkhela'         ,
'Battagram'         ,
#Birote
'Chakdara'           ,
'Charsadda'           ,
#Cherat
'Chitral'              ,
'Daggar'                ,
'Dargai'                 ,
'Dera Ismail Khan'        ,
#Doaba
'Dir'                      ,
'Drosh'                     ,
'Hangu'                      ,
'Haripur'                     ,
'Karak'                        ,
'Kohat'                         ,
'Kulachi'                        ,
'Lakki Marwat'                    ,
#Latamber
'Madyan'                           ,
'Mansehra'                          ,
'Mardan'                             ,
'Mastuj'                              ,
'Mingora'                              ,
'Naran, Kaghan Valley'                  ,
'Nowshera'                               ,
'Paharpur'                                ,
'Pabbi'                                    ,
'Peshawar'                                  ,
'Risalpur'                                   ,
'Saidu Sharif'                                ,
#Shewa Adda
'Swabi'                                        ,
'Swat'                                          ,
#Tangi
#Tank
#Thall
'Timergara'                                      ,
'Tordher'].each do |kp|

  City.create(name: kp,province: kpk  )
end









#################################################################################### punjab

punjab= Province.create(name: "Punjab" )

["Ahmadpur East",
"Ahmed Nager Chatha",
"Ali Khan Abad"      ,
"Alipur chatha Tehsil",
"Arifwala"             ,
"Attock"                ,
"Bhera"                  ,
"Bhalwal"                 ,
"Bahawalnagar"             ,
"Bahawalpur"                ,
"Bhakkar"                    ,
"Burewala"                    ,
"Chillianwala"                 ,
"Chakwal"                       ,
"Chichawatni"                    ,
"Chiniot"                         ,
"Chishtian",
"Daska"     ,
"Darya Khan" ,
"Dera Ghazi Khan" ,
"Dhaular"          ,
"Dina"              ,
"Dinga"              ,
"Dipalpur"            ,
"Faisalabad"           ,
"Fateh Jang"            ,
"Ghakhar Mandi"          ,
"Gojra"                   ,
"Gujranwala"               ,
"Gujrat city"               ,
"Gujar Khan"                 ,
"Hafizabad"                   ,
"Haroonabad"                   ,
"Hasilpur"                      ,
"Haveli Lakha"                   ,
"Jalalpur Jattan"                 ,
"Jampur"                           ,
"Jaranwala"                         ,
"Jhang"                              ,
"Jhelum"                              ,
"Kalabagh"                             ,
"Karor Lal Esan"                        ,
"Kasur"                                  ,
"Kamalia"                                 ,
"Kāmoke"                                   ,
"Khanewal"                                  ,
"Khanpur"                                    ,
"Kharian"                                     ,
"Khushab"                                      ,
"Kot Adu"                                       ,
"Jauharabad"                                     ,
"Lahore"                                          ,
"Lalamusa"                                         ,
"Layyah"                                            ,
"Liaquat Pur"                                        ,
"Lodhran"                                             ,
"Malakwal"                                             ,
"Mamoori"                                               ,
"Mailsi"                                                 ,
"Mandi Bahauddin"                                         ,
"Mian Channu"                                              ,
"Mianwali"                                                  ,
"Multan"                                                     ,
"Murree"                                                      ,
"Muridke"                                                      ,
"Mianwali Bangla"                                               ,
"Muzaffargarh"                                                   ,
"Narowal"                                                         ,
"Okara"                                                            ,
"Renala Khurd"                                                      ,
"Pakpattan"                                                          ,
"Pattoki"                                                             ,
"Pir Mahal"                                                            ,
"Qaimpur"                                                               ,
"Qila Didar Singh"                                                       ,
"Rabwah"                                                                  ,
"Raiwind"                                                                  ,
"Rajanpur"                                                                  ,
"Rahim Yar Khan"                                                             ,
"Rawalpindi"                                                                  ,
"Sadiqabad"                                                                    ,
"Safdarabad"                                                                    ,
"Sahiwal"                                                                        ,
"Sangla Hill"                                                                     ,
"Sarai Alamgir"                                                                    ,
"Sargodha"                                                                          ,
"Shakargarh"                                                                         ,
"Sheikhupura"                                                                         ,
"Sialkot"                                                                              ,
"Sohawa"                                                                                ,
"Soianwala"                                                                              ,
"Siranwali"                                                                               ,
"Talagang"                                                                                 ,
"Taxila"                                                                                    ,
"Toba Tek Singh"                                                                             ,
"Vehari"                                                                                      ,
"Wah Cantonment"                                                                               ,
"Wazirabad"].each do |p|

  City.create(name: p,province: punjab  )
end

######################## sindh

sindh = Province.create(name: "Sindh")


["Badin",
#Bhirkan
#Bhiria City
#Bhiria Road
#Rajo Khanani
#Chak
'Dadu'  ,
#Digri
#Diplo
#Dokri
#Gambat
#Garhi Yasin
'Ghotki'                                      ,
'Hyderabad',
'Islamkot'  ,
'Jacobabad'  ,
'Jamshoro'    ,
'Jungshahi'    ,
'Kandhkot'      ,
#Kandiaro
'Karachi'        ,
'Kashmore'        ,
#Keti Bandar
#Khadro
'Khairpur'         ,
#Khipro
#Korangi
#Kotri
'Larkana'           ,
#Malir
#Madeji
#Matiari
'Mehar'              ,
'Mirpur Khas'         ,
'Mithani'              ,
#Mithi
#Mehrabpur
'Moro'                  ,
'Nagarparkar'            ,
'Naudero'                 ,
'Naushahro Feroze'         ,
'Nawabshah'                 ,
'Qambar'                     ,
'Sijawal Junejo'              ,
'Qasimabad'                    ,
#Ranipur
#Ratodero

'Rohri'                         ,
#Sakrand
#Sanghar
#Shahbandar
#Shahdadkot
#Shahdadpur
#Shahpur Chakar
'Shikarpaur'                     ,
'Sinjhoro'                        ,
#Sukkur
#Tangwani
'Tando Adam Khan'                  ,
'Tando Allahyar'                    ,
'Tando Muhammad Khan'                ,
'Thatta'                              ,
#Thari Mirwah
'Umerkot'].each do |s|

  City.create(name: s,province: sindh  )
end
#Warah
#Piryaloi
#Tharushah
#Sita Road
#Pir Jo Goth
#Shahpur jahania
#Qubo Saeed Khan
#Mehrabpur
#Samaro
#Maqsoodo Rind
#Sann
#Manjhand
#Lakhi ghulam shah
#Mian Sahib
#Khairpur Nathan Shah
#Boriri

["Admin" ,"user"].each do |r|
  role = Role.create(name: r)

  if role.name == "Admin"
    usr = User.new(email: "admin@gmail.com",password: "11111111",password_confirmation: "11111111",role: role)
    usr.skip_confirmation!
    usr.save
  end

end




