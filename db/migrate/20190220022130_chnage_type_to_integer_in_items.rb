class ChnageTypeToIntegerInItems < ActiveRecord::Migration[5.2]
  def change
    change_column :items, :price, :integer, using: 'price::integer',default: 0
    # change_column :items, :price, 'integer USING CAST(price AS integer)',default: 0

    change_column :items, :vehicle_drive_in_km, :integer, using: 'vehicle_drive_in_km::integer',default: 0
    # change_column :items, :bed_rooms, :integer, using: 'bed_rooms::integer',default: 0
    # change_column :items, :bath_rooms, :integer, using: 'bath_rooms::integer',default: 0
    # change_column :items, :floor_level, :integer, using: 'floor_level::integer',default: 0
    change_column :items, :area, :integer, using: 'area::integer',default: 0

  end
end
