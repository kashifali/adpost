class AddFieldsToProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :avatar, :string
    add_column :profiles, :timings, :string
    add_column :profiles, :business_name, :string
    add_column :profiles, :office_address, :string
  end
end
