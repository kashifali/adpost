class AddSlug1ToCitiesSocieties < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :slug1, :string
    add_index :cities, :slug1, unique: true
  end
end
