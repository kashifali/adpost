class AddFacilitiesToItems < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :facilities, :text
  end
end
