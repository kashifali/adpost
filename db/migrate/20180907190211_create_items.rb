class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.string :name
      t.string :price
      t.string :vehicle_model
      t.string :vehicle_transaction_type
      t.string :vehicle_registered_in
      t.string :vehicle_year
      t.string :vehicle_fuel_type
      t.string :vehicle_drive_in_km
      t.string :vehicle_condition
      t.string :furnished
      t.string :bed_rooms
      t.string :bath_rooms
      t.string :floor_level
      t.string :area_unit
      t.string :area
      t.string :job_salary_period
      t.string :job_salary_from
      t.string :job_salary_to
      t.string :job_position_type
      t.text :description
      t.integer :status
      t.integer :user_id
      t.integer :sub_category_id
      t.integer :product_id
      t.integer :views_count

      t.timestamps
    end
  end
end
