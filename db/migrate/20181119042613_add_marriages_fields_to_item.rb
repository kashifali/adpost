class AddMarriagesFieldsToItem < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :age_from, :integer
    add_column :items, :age_to, :integer
    add_column :items, :height_feet, :integer
    add_column :items, :height_inch, :integer
    add_column :items, :religion, :string
    add_column :items, :country, :string
    add_column :items, :religious_sect, :string
    add_column :items, :education, :string
    add_column :items, :cast, :string
    add_column :items, :ethnic_background, :string
  end
end


