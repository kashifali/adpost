class AddCategoryIdAndIsFeaturedToItems < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :category_id, :integer
    add_column :items, :is_featured, :integer ,default: 0
  end
end
