class AddProviderToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :provider, :string
    add_column :users, :uid, :string
    remove_column :profiles, :first_name, :string
    remove_column :profiles, :last_name, :string
    add_column :profiles, :name, :string
    add_column :items, :full_name, :string #may use without lohin ad posting
    add_column :items, :phone, :string #may use without lohin ad posting
  end
end
