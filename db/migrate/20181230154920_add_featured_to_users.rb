class AddFeaturedToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :is_featured, :integer , default: 0
  end
end
