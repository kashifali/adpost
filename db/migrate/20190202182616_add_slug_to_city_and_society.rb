class AddSlugToCityAndSociety < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :slug, :string
    add_column :societies, :slug, :string
    add_index :cities, :slug, unique: true
    add_index :societies, :slug, unique: true
  end
end
