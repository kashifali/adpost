class AddLaneLineNumberToProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :line_number, :string
  end
end
