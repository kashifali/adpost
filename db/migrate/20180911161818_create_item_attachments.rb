class CreateItemAttachments < ActiveRecord::Migration[5.2]
  def change
    create_table :item_attachments do |t|
      t.integer :item_id
      t.string :avatar

      t.timestamps
    end
  end
end
