class CreateBlocks < ActiveRecord::Migration[5.2]
  def change
    create_table :blocks do |t|
      t.string :name
      t.string :block_slug
      t.integer :phase_sector_id

      t.timestamps
    end
  end
end
