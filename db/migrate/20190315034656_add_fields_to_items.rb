class AddFieldsToItems < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :block_id, :integer
    add_column :items, :phase_sector_id, :integer

    add_index :blocks, :block_slug, unique: true
    add_index :blocks, :name, unique: true

    add_index :phase_sectors, :phase_slug, unique: true
    add_index :phase_sectors, :name, unique: true


  end
end
