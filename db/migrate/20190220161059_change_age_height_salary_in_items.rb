class ChangeAgeHeightSalaryInItems < ActiveRecord::Migration[5.2]
  def change

    change_column :items, :age_from, :integer, using: 'age_from::integer',default: 0
    change_column :items, :age_to, :integer, using: 'age_to::integer',default: 0
    change_column :items, :height_feet, :integer, using: 'height_feet::integer',default: 0
    change_column :items, :height_inch, :integer, using: 'height_inch::integer',default: 0
    change_column :items, :job_salary_from, :integer, using: 'job_salary_from::integer',default: 0
    change_column :items, :job_salary_to, :integer, using: 'job_salary_to::integer',default: 0


  end
end

