class AddFieldsToBlocks < ActiveRecord::Migration[5.2]
  def change
    add_column :phase_sectors, :city_id, :integer
    add_column :blocks, :city_id, :integer
    add_column :blocks, :society_id, :integer
  end
end
