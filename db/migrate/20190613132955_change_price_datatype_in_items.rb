class ChangePriceDatatypeInItems < ActiveRecord::Migration[5.2]
  def change
    change_column :items, :price, :bigint
  end
end
