class AddCityIdAndIndexesToItems < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :city_id, :integer

    add_index :items, :name
    add_index :items, :vehicle_model
    add_index :items, :vehicle_transaction_type
    add_index :items, :vehicle_registered_in
    add_index :items, :vehicle_year
    add_index :items, :price
    add_index :items, :vehicle_fuel_type
    add_index :items, :vehicle_drive_in_km
    add_index :items, :vehicle_condition
    add_index :items, :furnished
    add_index :items, :bed_rooms
    add_index :items, :bath_rooms
    add_index :items, :floor_level
    add_index :items, :area_unit
    add_index :items, :area
    add_index :items, :job_salary_period
    add_index :items, :job_salary_from
    add_index :items, :job_salary_to
    add_index :items, :job_position_type
    add_index :items, :user_id
    add_index :items, :sub_category_id
    add_index :items, :product_id
    add_index :items, :city_id
    add_index :items, :status

  end
end
