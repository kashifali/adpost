class CreatePhaseSectors < ActiveRecord::Migration[5.2]
  def change
    create_table :phase_sectors do |t|
      t.string :name
      t.string :phase_slug
      t.integer :society_id

      t.timestamps
    end
  end
end
