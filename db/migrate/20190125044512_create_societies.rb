class CreateSocieties < ActiveRecord::Migration[5.2]
  def change
    create_table :societies do |t|
      t.string :name
      t.integer :city_id

      t.timestamps
    end
    add_column :items, :society_id, :integer
  end
end
