class RemoveIndexToItems < ActiveRecord::Migration[5.2]
  def change
    remove_index :items, :name
    add_index :items, :name

  end
end
